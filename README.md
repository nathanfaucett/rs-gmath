# rs-gmath

=====

math for graphics and games

```rust
extern crate gmath;

use gmath::{vec3, quat, mat4};

fn main() {
    let position = vec3::new_zero();
    let scale = vec3::new_one();
    let rotation = quat::new_identity();
    let mut matrix = mat4::new_identity();

    mat4::compose(&mut matrix, &position, &scale, &rotation);
}
```
