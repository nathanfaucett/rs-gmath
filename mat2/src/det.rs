use core::ops::{Mul, Sub};

/// # Example
/// ```
/// let mut m = mat2::new_identity::<f32>();
/// assert_eq!(mat2::det(&m), 1_f32);
/// ```
#[inline]
pub fn det<T>(out: &[T; 4]) -> T
where
    T: Sub<T, Output = T>,
    for<'a, 'b> &'a T: Mul<&'b T, Output = T>,
{
    &out[0] * &out[3] - &out[2] * &out[1]
}
