use core::ops::{Div, Mul, Neg, Sub};

use num_traits::{One, Zero};

use super::set_identity;

/// # Example
/// ```
/// let mut m = mat2::new_identity::<f32>();
/// mat2::inv(&mut m, &mat2::new_identity());
/// assert_eq!(m, mat2::new_identity::<f32>());
/// ```
#[inline]
pub fn inv<'out, T>(out: &'out mut [T; 4], m: &[T; 4]) -> &'out mut [T; 4]
where
    T: One + Zero + Sub<T, Output = T>,
    for<'a, 'b> &'a T: Neg<Output = T> + Mul<&'b T, Output = T> + Div<&'b T, Output = T>,
{
    let m00 = &m[0];
    let m10 = &m[1];
    let m01 = &m[2];
    let m11 = &m[3];
    let d = m00 * m11 - m10 * m01;

    if d.is_zero() {
        set_identity(out)
    } else {
        let inv_d = &T::one() / &d;

        out[0] = m11 * &inv_d;
        out[1] = &-m01 * &inv_d;
        out[2] = &-m10 * &inv_d;
        out[3] = m00 * &inv_d;
        out
    }
}
/// # Example
/// ```
/// let mut m = mat2::new_identity::<f32>();
/// mat2::inv_mut(&mut m);
/// assert_eq!(m, mat2::new_identity::<f32>());
/// ```
#[inline]
pub fn inv_mut<'out, T>(out: &'out mut [T; 4]) -> &'out mut [T; 4]
where
    T: Clone + One + Zero + Sub<T, Output = T>,
    for<'a, 'b> &'a T: Neg<Output = T> + Mul<&'b T, Output = T> + Div<&'b T, Output = T>,
{
    let tmp = out.clone();
    inv(out, &tmp)
}
