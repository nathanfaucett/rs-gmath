use num_traits::{One, Zero};

#[inline(always)]
pub fn new<T>(m00: T, m01: T, m10: T, m11: T) -> [T; 4] {
    [m00, m10, m01, m11]
}

#[inline(always)]
pub fn new_identity<T>() -> [T; 4]
where
    T: One + Zero,
{
    new(T::one(), T::zero(), T::zero(), T::one())
}

#[inline(always)]
pub fn new_one<T>() -> [T; 4]
where
    T: One,
{
    new(T::one(), T::one(), T::one(), T::one())
}

#[inline(always)]
pub fn new_zero<T>() -> [T; 4]
where
    T: Zero,
{
    new(T::zero(), T::zero(), T::zero(), T::zero())
}
