use core::ops::{Add, Div, Mul, Neg, Sub};

use num_traits::{One, Zero};

use super::inv;

/// # Example
/// ```
/// let mut m = mat2::new_identity::<f32>();
/// mat2::mul(&mut m, &mat2::new_identity(), &mat2::new_identity());
/// assert_eq!(m, mat2::new_identity::<f32>());
/// ```
#[inline]
pub fn mul<'out, T>(out: &'out mut [T; 4], a: &[T; 4], b: &[T; 4]) -> &'out mut [T; 4]
where
    T: Add<T, Output = T>,
    for<'a, 'b> &'a T: Mul<&'b T, Output = T>,
{
    out[0] = &a[0] * &b[0] + &a[1] * &b[2];
    out[1] = &a[2] * &b[0] + &a[3] * &b[2];
    out[2] = &a[0] * &b[1] + &a[1] * &b[3];
    out[3] = &a[2] * &b[1] + &a[3] * &b[3];
    out
}
/// rmul_mut(A, B) does A = B * A
/// # Example
/// ```
/// let mut m = mat2::new_identity::<f32>();
/// mat2::rmul_mut(&mut m, &mat2::new_identity());
/// assert_eq!(m, mat2::new_identity::<f32>());
/// ```
#[inline]
pub fn rmul_mut<'out, T>(out: &'out mut [T; 4], m: &[T; 4]) -> &'out mut [T; 4]
where
    T: Clone + Add<T, Output = T>,
    for<'a, 'b> &'a T: Mul<&'b T, Output = T>,
{
    let tmp = out.clone();
    mul(out, m, &tmp)
}
/// lmul_mut(A, B) does A = A * B
/// # Example
/// ```
/// let mut m = mat2::new_identity::<f32>();
/// mat2::lmul_mut(&mut m, &mat2::new_identity());
/// assert_eq!(m, mat2::new_identity::<f32>());
/// ```
#[inline]
pub fn lmul_mut<'out, T>(out: &'out mut [T; 4], m: &[T; 4]) -> &'out mut [T; 4]
where
    T: Clone + Add<T, Output = T>,
    for<'a, 'b> &'a T: Mul<&'b T, Output = T>,
{
    let tmp = out.clone();
    mul(out, &tmp, m)
}
/// # Example
/// ```
/// let mut m = mat2::new_identity::<f32>();
/// mat2::smul(&mut m, &mat2::new_identity(), &1_f32);
/// assert_eq!(m, mat2::new_identity::<f32>());
/// ```
#[inline]
pub fn smul<'out, T>(out: &'out mut [T; 4], m: &[T; 4], s: &T) -> &'out mut [T; 4]
where
    T: Add<T, Output = T>,
    for<'a, 'b> &'a T: Mul<&'b T, Output = T>,
{
    out[0] = &m[0] * s;
    out[1] = &m[1] * s;
    out[2] = &m[2] * s;
    out[3] = &m[3] * s;
    out
}
/// # Example
/// ```
/// let mut m = mat2::new_identity::<f32>();
/// mat2::smul_mut(&mut m, &1_f32);
/// assert_eq!(m, mat2::new_identity::<f32>());
/// ```
#[inline]
pub fn smul_mut<'out, T>(out: &'out mut [T; 4], s: &T) -> &'out mut [T; 4]
where
    T: Clone + Add<T, Output = T>,
    for<'a, 'b> &'a T: Mul<&'b T, Output = T>,
{
    let tmp = out.clone();
    smul(out, &tmp, s)
}
/// # Example
/// ```
/// let mut m = mat2::new_identity::<f32>();
/// mat2::div(&mut m, &mat2::new_identity(), &mat2::new_identity());
/// assert_eq!(m, mat2::new_identity::<f32>());
/// ```
#[inline]
pub fn div<'out, T>(out: &'out mut [T; 4], a: &[T; 4], b: &[T; 4]) -> &'out mut [T; 4]
where
    T: Clone + One + Zero + Sub<T, Output = T>,
    for<'a, 'b> &'a T: Neg<Output = T> + Mul<&'b T, Output = T> + Div<&'b T, Output = T>,
{
    let mut inv_b = b.clone();
    inv(&mut inv_b, b);
    mul(out, a, &inv_b)
}
/// rdiv_mut(A, B) does A = B / A
/// # Example
/// ```
/// let mut m = mat2::new_identity::<f32>();
/// mat2::rdiv_mut(&mut m, &mat2::new_identity());
/// assert_eq!(m, mat2::new_identity::<f32>());
/// ```
#[inline]
pub fn rdiv_mut<'out, T>(out: &'out mut [T; 4], m: &[T; 4]) -> &'out mut [T; 4]
where
    T: Clone + One + Zero + Sub<T, Output = T>,
    for<'a, 'b> &'a T: Neg<Output = T> + Mul<&'b T, Output = T> + Div<&'b T, Output = T>,
{
    let tmp = out.clone();
    div(out, m, &tmp)
}
/// ldiv_mut(A, B) does A = A / B
/// # Example
/// ```
/// let mut m = mat2::new_identity::<f32>();
/// mat2::ldiv_mut(&mut m, &mat2::new_identity());
/// assert_eq!(m, mat2::new_identity::<f32>());
/// ```
#[inline]
pub fn ldiv_mut<'out, T>(out: &'out mut [T; 4], m: &[T; 4]) -> &'out mut [T; 4]
where
    T: Clone + One + Zero + Sub<T, Output = T>,
    for<'a, 'b> &'a T: Neg<Output = T> + Mul<&'b T, Output = T> + Div<&'b T, Output = T>,
{
    let tmp = out.clone();
    div(out, &tmp, m)
}
/// # Example
/// ```
/// let mut m = mat2::new_identity::<f32>();
/// mat2::sdiv(&mut m, &mat2::new_identity(), &1_f32);
/// assert_eq!(m, mat2::new_identity::<f32>());
/// ```
#[inline]
pub fn sdiv<'out, T>(out: &'out mut [T; 4], m: &[T; 4], s: &T) -> &'out mut [T; 4]
where
    T: One + Zero + Add<T, Output = T>,
    for<'a, 'b> &'a T: Div<&'b T, Output = T> + Mul<&'b T, Output = T>,
{
    let inv_s = if s.is_zero() {
        T::zero()
    } else {
        &T::one() / s
    };
    smul(out, m, &inv_s)
}
/// # Example
/// ```
/// let mut m = mat2::new_identity::<f32>();
/// mat2::sdiv_mut(&mut m, &1_f32);
/// assert_eq!(m, mat2::new_identity::<f32>());
/// ```
#[inline]
pub fn sdiv_mut<'out, T>(out: &'out mut [T; 4], s: &T) -> &'out mut [T; 4]
where
    T: Clone + One + Zero + Add<T, Output = T>,
    for<'a, 'b> &'a T: Div<&'b T, Output = T> + Mul<&'b T, Output = T>,
{
    let tmp = out.clone();
    sdiv(out, &tmp, s)
}
