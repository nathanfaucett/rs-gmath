use num_traits::{One, Zero};

#[inline]
pub fn set<T>(out: &mut [T; 4], m00: T, m01: T, m10: T, m11: T) -> &mut [T; 4] {
    out[0] = m00;
    out[1] = m10;
    out[2] = m01;
    out[3] = m11;
    out
}

#[inline(always)]
pub fn set_identity<T>(out: &mut [T; 4]) -> &mut [T; 4]
where
    T: One + Zero,
{
    set(out, T::one(), T::zero(), T::zero(), T::one())
}

#[inline(always)]
pub fn set_zero<T>(out: &mut [T; 4]) -> &mut [T; 4]
where
    T: Zero,
{
    set(out, T::zero(), T::zero(), T::zero(), T::zero())
}

#[inline(always)]
pub fn set_one<T>(out: &mut [T; 4]) -> &mut [T; 4]
where
    T: One,
{
    set(out, T::one(), T::one(), T::one(), T::one())
}

#[inline]
pub fn set_mat32<'out, T>(out: &'out mut [T; 4], m: &[T; 6]) -> &'out mut [T; 4]
where
    T: One + Zero + Clone,
{
    set(out, m[0].clone(), m[2].clone(), m[1].clone(), m[3].clone())
}

#[inline]
pub fn set_mat3<'out, T>(out: &'out mut [T; 4], m: &[T; 9]) -> &'out mut [T; 4]
where
    T: One + Zero + Clone,
{
    set(out, m[0].clone(), m[3].clone(), m[1].clone(), m[4].clone())
}

#[inline]
pub fn set_mat4<'out, T>(out: &'out mut [T; 4], m: &[T; 16]) -> &'out mut [T; 4]
where
    T: One + Zero + Clone,
{
    set(out, m[0].clone(), m[4].clone(), m[1].clone(), m[5].clone())
}
