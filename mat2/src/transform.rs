use core::ops::{Add, Mul, Neg};

use num_traits::Float;

/// # Example
/// ```
/// use std::f32;
/// let mut m = mat2::new_identity::<f32>();
/// mat2::set_rotation(&mut m, &f32::consts::FRAC_PI_2);
/// assert_eq!(m[1], 1f32);
/// assert_eq!(m[2], -1f32);
/// ```
#[inline]
pub fn set_rotation<'out, T>(out: &'out mut [T; 4], angle: &T) -> &'out mut [T; 4]
where
    T: Float + Neg<Output = T>,
{
    let c = angle.cos();
    let s = angle.sin();

    out[0] = c;
    out[1] = s;
    out[2] = -s;
    out[3] = c;
    out
}
/// # Example
/// ```
/// use std::f32;
/// let mut m = mat2::new_identity::<f32>();
/// mat2::set_rotation(&mut m, &f32::consts::FRAC_PI_2);
/// assert_eq!(mat2::rotation(&m), f32::consts::FRAC_PI_2);
/// ```
#[inline]
pub fn rotation<'out, T>(out: &[T; 4]) -> T
where
    T: Clone + Float,
{
    out[1].atan2(out[0].clone())
}
/// # Example
/// ```
/// use std::f32;
/// let mut m = mat2::new_identity::<f32>();
/// mat2::rotate(&mut m, &mat2::new_identity::<f32>(), &f32::consts::FRAC_PI_2,);
/// assert_eq!(m[1], 1f32);
/// assert_eq!(m[2], -1f32);
/// ```
#[inline]
pub fn rotate<'out, T>(out: &'out mut [T; 4], a: &[T; 4], angle: &T) -> &'out mut [T; 4]
where
    T: Float + Add<T, Output = T>,
    for<'a, 'b> &'a T: Mul<&'b T, Output = T> + Neg<Output = T>,
{
    let m11 = &a[0];
    let m12 = &a[2];
    let m21 = &a[1];
    let m22 = &a[3];
    let c = angle.cos();
    let s = angle.sin();

    out[0] = m11 * &c + m12 * &-&s;
    out[1] = m11 * &s + m12 * &c;
    out[2] = m21 * &c + m22 * &-&s;
    out[3] = m21 * &s + m22 * &c;
    out
}
