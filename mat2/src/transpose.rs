/// # Example
/// ```
/// let mut m = mat2::new_identity::<f32>();
/// mat2::transpose(&mut m, &mat2::new_identity::<f32>());
/// assert_eq!(m, mat2::new_identity::<f32>());
/// ```
#[inline]
pub fn transpose<'out, T>(out: &'out mut [T; 4], a: &[T; 4]) -> &'out mut [T; 4]
where
    T: Clone,
{
    out[0] = a[0].clone();
    out[1] = a[2].clone();
    out[2] = a[1].clone();
    out[3] = a[3].clone();
    out
}
