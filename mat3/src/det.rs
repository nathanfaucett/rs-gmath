use core::ops::{Add, Mul, Sub};

/// # Example
/// ```
/// let mut m = mat3::new_identity::<f32>();
/// assert_eq!(mat3::det(&m), 1_f32);
/// ```
#[inline]
pub fn det<T>(out: &[T; 9]) -> T
where
    T: Add<T, Output = T> + Sub<T, Output = T>,
    for<'a, 'b> &'a T: Mul<&'b T, Output = T>,
{
    let a = &out[0];
    let b = &out[1];
    let c = &out[2];
    let d = &out[3];
    let e = &out[4];
    let f = &out[5];
    let g = &out[6];
    let h = &out[7];
    let i = &out[8];
    return &(a * e) * i - &(a * f) * h - &(b * d) * i + &(b * f) * g + &(c * d) * h - &(c * e) * g;
}
