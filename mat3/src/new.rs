use num_traits::{One, Zero};

#[inline(always)]
pub fn new<T>(m00: T, m01: T, m02: T, m10: T, m11: T, m12: T, m20: T, m21: T, m22: T) -> [T; 9] {
    [m00, m10, m20, m01, m11, m21, m02, m12, m22]
}

#[inline(always)]
pub fn new_identity<T>() -> [T; 9]
where
    T: One + Zero,
{
    new(
        T::one(),
        T::zero(),
        T::zero(),
        T::zero(),
        T::one(),
        T::zero(),
        T::zero(),
        T::zero(),
        T::one(),
    )
}

#[inline(always)]
pub fn new_one<T>() -> [T; 9]
where
    T: One,
{
    new(
        T::one(),
        T::one(),
        T::one(),
        T::one(),
        T::one(),
        T::one(),
        T::one(),
        T::one(),
        T::one(),
    )
}

#[inline(always)]
pub fn new_zero<T>() -> [T; 9]
where
    T: Zero,
{
    new(
        T::zero(),
        T::zero(),
        T::zero(),
        T::zero(),
        T::zero(),
        T::zero(),
        T::zero(),
        T::zero(),
        T::zero(),
    )
}
