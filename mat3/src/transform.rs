use core::ops::Mul;

#[inline]
pub fn scale<'out, T>(out: &'out mut [T; 9], a: &[T; 9], v: &[T; 3]) -> &'out mut [T; 9]
where
    T: Clone,
    for<'a, 'b> &'a T: Mul<&'b T, Output = T>,
{
    let x = &v[0];
    let y = &v[1];
    let z = &v[2];

    out[0] = &a[0] * x;
    out[3] = &a[3] * y;
    out[6] = &a[6] * z;
    out[1] = &a[1] * x;
    out[4] = &a[4] * y;
    out[7] = &a[7] * z;
    out[2] = &a[2] * x;
    out[5] = &a[5] * y;
    out[8] = &a[8] * z;
    out
}
