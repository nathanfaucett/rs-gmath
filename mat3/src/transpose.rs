/// # Example
/// ```
/// let mut m = mat3::new_identity::<f32>();
/// mat3::transpose(&mut m, &mat3::new_identity::<f32>());
/// assert_eq!(m, mat3::new_identity::<f32>());
/// ```
#[inline]
pub fn transpose<'out, T>(out: &'out mut [T; 9], a: &[T; 9]) -> &'out mut [T; 9]
where
    T: Clone,
{
    out[0] = a[0].clone();
    out[1] = a[3].clone();
    out[2] = a[6].clone();
    out[3] = a[1].clone();
    out[4] = a[4].clone();
    out[5] = a[7].clone();
    out[6] = a[2].clone();
    out[7] = a[5].clone();
    out[8] = a[8].clone();
    out
}
