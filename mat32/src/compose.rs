use core::ops::{Mul, Neg};

use num_traits::Float;
use vec2;

/// # Example
/// ```
/// let mut m = mat32::new_identity();
/// let position = [0f32, 0f32];
/// let scale = [1f32, 1f32];
/// let rotation = 0f32;
/// mat32::compose(&mut m, &position, &scale, &rotation);
/// assert_eq!(m, mat32::new_identity());
/// ```
#[inline]
pub fn compose<'out, T>(
    out: &'out mut [T; 6],
    position: &[T; 2],
    scale: &[T; 2],
    rotation: &T,
) -> &'out mut [T; 6]
where
    T: Clone + Float,
    for<'a, 'b> &'a T: Mul<&'b T, Output = T> + Neg<Output = T>,
{
    let sx = &scale[0];
    let sy = &scale[1];
    let c = rotation.cos();
    let s = rotation.sin();

    out[0] = &c * sx;
    out[1] = &s * sx;
    out[2] = &-&s * sy;
    out[3] = &c * sy;
    out[4] = position[0].clone();
    out[5] = position[1].clone();
    out
}

/// # Example
/// ```
/// let m = mat32::new_identity();
/// let mut position = [0f32, 0f32];
/// let mut scale = [1f32, 1f32];
/// let mut rotation = 0f32;
/// mat32::decompose(&m, &mut position, &mut scale, &mut rotation);
/// assert_eq!(position, [0f32, 0f32]);
/// assert_eq!(scale, [1f32, 1f32]);
/// assert_eq!(rotation, 0f32);
/// ```
#[inline]
pub fn decompose<T>(out: &[T; 6], position: &mut [T; 2], scale: &mut [T; 2], rotation: &mut T)
where
    T: Clone + Float,
    for<'a, 'b> &'a T: Mul<&'b T, Output = T> + Neg<Output = T>,
{
    let sx = vec2::len_values(&out[0], &out[1]);
    let sy = vec2::len_values(&out[2], &out[3]);

    position[0] = out[4].clone();
    position[1] = out[5].clone();

    scale[0] = sx;
    scale[1] = sy;

    *rotation = out[1].atan2(out[0].clone());
}
