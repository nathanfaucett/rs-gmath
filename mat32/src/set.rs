use num_traits::{One, Zero};

#[inline]
pub fn set<T>(out: &mut [T; 6], m00: T, m01: T, m02: T, m10: T, m11: T, m12: T) -> &mut [T; 6] {
    out[0] = m00;
    out[2] = m10;
    out[4] = m02;
    out[1] = m01;
    out[3] = m11;
    out[5] = m12;
    out
}

#[inline(always)]
pub fn set_identity<T>(out: &mut [T; 6]) -> &mut [T; 6]
where
    T: One + Zero,
{
    set(
        out,
        T::one(),
        T::zero(),
        T::zero(),
        T::zero(),
        T::one(),
        T::zero(),
    )
}

#[inline(always)]
pub fn set_zero<T>(out: &mut [T; 6]) -> &mut [T; 6]
where
    T: Zero,
{
    set(
        out,
        T::zero(),
        T::zero(),
        T::zero(),
        T::zero(),
        T::zero(),
        T::zero(),
    )
}

#[inline(always)]
pub fn set_one<T>(out: &mut [T; 6]) -> &mut [T; 6]
where
    T: One,
{
    set(
        out,
        T::one(),
        T::one(),
        T::one(),
        T::one(),
        T::one(),
        T::one(),
    )
}

#[inline]
pub fn set_mat2<'out, T>(out: &'out mut [T; 6], m: &[T; 4]) -> &'out mut [T; 6]
where
    T: Zero + Clone,
{
    set(
        out,
        m[0].clone(),
        m[2].clone(),
        T::zero(),
        m[1].clone(),
        m[3].clone(),
        T::zero(),
    )
}

#[inline]
pub fn set_mat3<'out, T>(out: &'out mut [T; 6], m: &[T; 9]) -> &'out mut [T; 6]
where
    T: Zero + Clone,
{
    set(
        out,
        m[0].clone(),
        m[3].clone(),
        T::zero(),
        m[1].clone(),
        m[4].clone(),
        T::zero(),
    )
}

#[inline]
pub fn set_mat4<'out, T>(out: &'out mut [T; 6], m: &[T; 16]) -> &'out mut [T; 6]
where
    T: Clone,
{
    set(
        out,
        m[0].clone(),
        m[4].clone(),
        m[12].clone(),
        m[1].clone(),
        m[5].clone(),
        m[13].clone(),
    )
}
