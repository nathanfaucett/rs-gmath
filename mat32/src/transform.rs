use core::f64;
use core::ops::{Add, Div, Mul, Neg, Sub};

use num_traits::{Float, FromPrimitive, Zero};

/// # Example
/// ```
/// use std::f32;
/// let mut m = mat32::new_identity::<f32>();
/// mat32::set_rotation(&mut m, &f32::consts::FRAC_PI_2);
/// assert_eq!(m[1], 1f32);
/// assert_eq!(m[2], -1f32);
/// ```
#[inline]
pub fn set_rotation<'out, T>(out: &'out mut [T; 6], angle: &T) -> &'out mut [T; 6]
where
    T: Float + Neg<Output = T>,
{
    let c = angle.cos();
    let s = angle.sin();

    out[0] = c;
    out[1] = s;
    out[2] = -s;
    out[3] = c;
    out
}
/// # Example
/// ```
/// use std::f32;
/// let mut m = mat32::new_identity::<f32>();
/// mat32::set_rotation(&mut m, &f32::consts::FRAC_PI_2);
/// assert_eq!(mat32::rotation(&m), f32::consts::FRAC_PI_2);
/// ```
#[inline]
pub fn rotation<'out, T>(out: &[T; 6]) -> T
where
    T: Clone + Float,
{
    out[1].atan2(out[0].clone())
}
/// # Example
/// ```
/// use std::f32;
/// let mut m = mat32::new_identity::<f32>();
/// mat32::rotate(&mut m, &mat32::new_identity::<f32>(), &f32::consts::FRAC_PI_2,);
/// assert_eq!(m[1], 1f32);
/// assert_eq!(m[2], -1f32);
/// ```
#[inline]
pub fn rotate<'out, T>(out: &'out mut [T; 6], a: &[T; 6], angle: &T) -> &'out mut [T; 6]
where
    T: Float + Add<T, Output = T>,
    for<'a, 'b> &'a T: Mul<&'b T, Output = T> + Neg<Output = T>,
{
    let m11 = &a[0];
    let m12 = &a[2];
    let m21 = &a[1];
    let m22 = &a[3];
    let c = angle.cos();
    let s = angle.sin();

    out[0] = m11 * &c + m12 * &-&s;
    out[1] = m11 * &s + m12 * &c;
    out[2] = m21 * &c + m22 * &-&s;
    out[3] = m21 * &s + m22 * &c;
    out
}
/// # Example
/// ```
/// let mut m = mat32::new_identity::<f32>();
/// mat32::look_at(&mut m, &[0f32, 0f32], &[0f32, 1f32]);
/// assert_eq!(m, [1f32, 0f32, 0f32, 1f32, 0f32, 0f32]);
/// ```
#[inline]
pub fn look_at<'out, T>(out: &'out mut [T; 6], eye: &[T; 2], target: &[T; 2]) -> &'out mut [T; 6]
where
    T: Float + FromPrimitive + Add<T, Output = T>,
    for<'a, 'b> &'a T: Mul<&'b T, Output = T> + Sub<&'b T, Output = T> + Neg<Output = T>,
{
    let x = &target[0] - &eye[0];
    let y = &target[1] - &eye[1];
    let a = &y.atan2(x) - &T::from_f64(f64::consts::FRAC_PI_2).unwrap();
    let c = a.cos();
    let s = a.sin();

    out[0] = c.clone();
    out[1] = s.clone();
    out[2] = -&s;
    out[3] = c;
    out
}
/// # Example
/// ```
/// let mut m = mat32::new_identity::<f32>();
/// mat32::set_position(&mut m, &[1f32, 1f32]);
/// assert_eq!(m[4], 1f32);
/// assert_eq!(m[5], 1f32);
/// ```
#[inline]
pub fn set_position<'out, T>(out: &'out mut [T; 6], position: &[T; 2]) -> &'out mut [T; 6]
where
    T: Clone,
{
    out[4] = position[1].clone();
    out[5] = position[1].clone();
    out
}
/// # Example
/// ```
/// let mut m = mat32::new_identity::<f32>();
/// mat32::translate(&mut m, &mat32::new_identity::<f32>(), &[1f32, 1f32]);
/// assert_eq!(m[4], 1f32);
/// assert_eq!(m[5], 1f32);
/// ```
#[inline]
pub fn translate<'out, T>(out: &'out mut [T; 6], a: &[T; 6], v: &[T; 2]) -> &'out mut [T; 6]
where
    T: Clone + Add<T, Output = T>,
    for<'a, 'b> &'a T: Mul<&'b T, Output = T>,
{
    out[4] = &a[0] * &v[0] + &a[2] * &v[1] + a[4].clone();
    out[5] = &a[1] * &v[0] + &a[3] * &v[1] + a[5].clone();
    out
}
/// # Example
/// ```
/// let mut m = mat32::new_identity::<f32>();
/// mat32::scale(&mut m, &mat32::new_identity::<f32>(), &[2f32, 2f32]);
/// assert_eq!(m[0], 2f32);
/// assert_eq!(m[3], 2f32);
/// ```
#[inline]
pub fn scale<'out, T>(out: &'out mut [T; 6], a: &[T; 6], v: &[T; 2]) -> &'out mut [T; 6]
where
    for<'a, 'b> &'a T: Mul<&'b T, Output = T>,
{
    let x = &v[0];
    let y = &v[1];

    out[0] = &a[0] * x;
    out[1] = &a[1] * x;

    out[2] = &a[2] * y;
    out[3] = &a[3] * y;

    out
}
/// # Example
/// ```
/// let mut m = mat32::new_identity::<f32>();
/// mat32::orthographic(&mut m, &-0.5f32, &0.5f32, &0.5f32, &-0.5f32);
/// ```
#[inline]
pub fn orthographic<'out, T>(
    out: &'out mut [T; 6],
    top: &T,
    right: &T,
    bottom: &T,
    left: &T,
) -> &'out mut [T; 6]
where
    T: Zero + FromPrimitive,
    for<'a, 'b> &'a T: Mul<&'b T, Output = T>
        + Add<&'b T, Output = T>
        + Sub<&'b T, Output = T>
        + Neg<Output = T>
        + Div<&'b T, Output = T>,
{
    let w = right - left;
    let h = top - bottom;

    let x = &(right + left) / &w;
    let y = &(top + bottom) / &h;

    out[0] = &T::from_usize(2).unwrap() / &w;
    out[1] = T::zero();
    out[2] = T::zero();
    out[3] = &T::from_usize(2).unwrap() / &h;
    out[4] = -&x;
    out[5] = -&y;

    out
}
