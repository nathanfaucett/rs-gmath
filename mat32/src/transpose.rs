/// # Example
/// ```
/// let mut m = mat32::new_identity::<f32>();
/// mat32::transpose(&mut m, &mat32::new_identity::<f32>());
/// assert_eq!(m, mat32::new_identity::<f32>());
/// ```
#[inline]
pub fn transpose<'out, T>(out: &'out mut [T; 6], a: &[T; 6]) -> &'out mut [T; 6]
where
    T: Clone,
{
    out[0] = a[0].clone();
    out[1] = a[2].clone();
    out[2] = a[1].clone();
    out[3] = a[3].clone();
    out
}
