use core::ops::{Add, Mul, Sub};

/// # Example
/// ```
/// let mut m = mat4::new_identity::<f32>();
/// assert_eq!(mat4::det(&m), 1_f32);
/// ```
#[inline]
pub fn det<T>(out: &[T; 16]) -> T
where
    T: Add<T, Output = T> + Sub<T, Output = T>,
    for<'a, 'b> &'a T: Mul<&'b T, Output = T>,
{
    let a00 = &out[0];
    let a01 = &out[1];
    let a02 = &out[2];
    let a03 = &out[3];
    let a10 = &out[4];
    let a11 = &out[5];
    let a12 = &out[6];
    let a13 = &out[7];
    let a20 = &out[8];
    let a21 = &out[9];
    let a22 = &out[10];
    let a23 = &out[11];
    let a30 = &out[12];
    let a31 = &out[13];
    let a32 = &out[14];
    let a33 = &out[15];

    let b00 = a00 * a11 - a01 * a10;
    let b01 = a00 * a12 - a02 * a10;
    let b02 = a00 * a13 - a03 * a10;
    let b03 = a01 * a12 - a02 * a11;
    let b04 = a01 * a13 - a03 * a11;
    let b05 = a02 * a13 - a03 * a12;
    let b06 = a20 * a31 - a21 * a30;
    let b07 = a20 * a32 - a22 * a30;
    let b08 = a20 * a33 - a23 * a30;
    let b09 = a21 * a32 - a22 * a31;
    let b10 = a21 * a33 - a23 * a31;
    let b11 = a22 * a33 - a23 * a32;

    &b00 * &b11 - &b01 * &b10 + &b02 * &b09 + &b03 * &b08 - &b04 * &b07 + &b05 * &b06
}
