/// # Example
/// ```
/// let mut m = mat4::new_identity::<f32>();
/// mat4::transpose(&mut m, &mat4::new_identity::<f32>());
/// assert_eq!(m, mat4::new_identity::<f32>());
/// ```
#[inline]
pub fn transpose<'out, T>(out: &'out mut [T; 16], a: &[T; 16]) -> &'out mut [T; 16]
where
    T: Clone,
{
    out[0] = a[0].clone();
    out[1] = a[4].clone();
    out[2] = a[8].clone();
    out[3] = a[12].clone();
    out[4] = a[1].clone();
    out[5] = a[5].clone();
    out[6] = a[9].clone();
    out[7] = a[13].clone();
    out[8] = a[2].clone();
    out[9] = a[6].clone();
    out[10] = a[10].clone();
    out[11] = a[14].clone();
    out[12] = a[3].clone();
    out[13] = a[7].clone();
    out[14] = a[11].clone();
    out[15] = a[15].clone();
    out
}
