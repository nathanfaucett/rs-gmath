# quat

quaternion functions

```rust
extern crate quat;

fn main() {
    let mut out = quat::new_identity();
    quat::set_axis_angle(&mut out, &[0.0, 0.0, 1.0], &1.5);
}
```
