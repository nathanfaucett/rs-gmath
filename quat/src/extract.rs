use num_traits::real::Real;
use num_traits::FromPrimitive;

/// # Example
/// ```
/// use std::f32::consts::FRAC_2_PI;
/// let mut axis = [0_f32; 3];
/// let angle = quat::axis_angle(&[0_f32, 0_f32, 0.3129618_f32, 0.94976574_f32], &mut axis);
/// assert_eq!(axis, [0_f32, 0_f32, 1.0528915_f32]);
/// assert_eq!(angle, 0.6366196);
/// ```
#[inline]
pub fn axis_angle<'out, T>(q: &[T; 4], out: &'out mut [T; 3]) -> T
where
    T: Real + FromPrimitive,
{
    let angle = q[3].acos() * T::from_usize(2).unwrap();
    let s = angle.sin() / T::from_usize(2).unwrap();

    if s != T::zero() {
        out[0] = q[0] / s;
        out[1] = q[1] / s;
        out[2] = q[2] / s;
    } else {
        out[0] = T::zero();
        out[1] = T::zero();
        out[2] = T::one();
    }

    angle
}
