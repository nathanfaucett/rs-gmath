use core::ops::Neg;

/// # Example
/// ```
/// let mut v = [0; 4];
/// quat::conj(&mut v, &[1; 4]);
/// assert_eq!(v, [-1, -1, -1, 1]);
/// ```
#[inline]
pub fn conj<'out, T>(out: &'out mut [T; 4], q: &[T; 4]) -> &'out mut [T; 4]
where
    T: Clone,
    for<'a, 'b> &'a T: Neg<Output = T>,
{
    out[0] = -&q[0];
    out[1] = -&q[1];
    out[2] = -&q[2];
    out[3] = q[3].clone();
    out
}
/// # Example
/// ```
/// let mut v = [0; 4];
/// quat::inv(&mut v, &[1; 4]);
/// assert_eq!(v, [-1, -1, -1, 1]);
/// ```
#[inline(always)]
pub fn inv<'out, T>(out: &'out mut [T; 4], q: &[T; 4]) -> &'out mut [T; 4]
where
    T: Clone,
    for<'a, 'b> &'a T: Neg<Output = T>,
{
    conj(out, q)
}
/// # Example
/// ```
/// let mut v = [1; 4];
/// quat::conj_mut(&mut v);
/// assert_eq!(v, [-1, -1, -1, 1]);
/// ```
#[inline]
pub fn conj_mut<'out, T>(out: &'out mut [T; 4]) -> &'out mut [T; 4]
where
    T: Clone,
    for<'a, 'b> &'a T: Neg<Output = T>,
{
    let tmp = out.clone();
    conj(out, &tmp)
}
/// # Example
/// ```
/// let mut v = [1; 4];
/// quat::inv_mut(&mut v);
/// assert_eq!(v, [-1, -1, -1, 1]);
/// ```
#[inline]
pub fn inv_mut<'out, T>(out: &'out mut [T; 4]) -> &'out mut [T; 4]
where
    T: Clone,
    for<'a, 'b> &'a T: Neg<Output = T>,
{
    conj_mut(out)
}
