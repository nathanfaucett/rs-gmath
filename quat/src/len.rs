use core::ops::Mul;

pub use vec4::dot;
pub use vec4::dot_values;
pub use vec4::inv_len;
pub use vec4::inv_len_values;
pub use vec4::len;
pub use vec4::len_sq;
pub use vec4::len_values;
pub use vec4::len_values_sq;

use num_traits::real::Real;

/// # Example
/// ```
/// let mut v = [0_f32; 4];
/// quat::w(&mut v, &[1_f32; 4]);
/// assert_eq!(v, [1_f32, 1_f32, 1_f32, 2_f32.sqrt()]);
/// ```
#[inline]
pub fn w<'out, T>(out: &'out mut [T; 4], q: &[T; 4]) -> &'out mut [T; 4]
where
    T: Real,
    for<'a, 'b> &'a T: Mul<&'b T, Output = T>,
{
    *out = *q;
    w_mut(out)
}
/// # Example
/// ```
/// let mut v = [1_f32; 4];
/// quat::w_mut(&mut v);
/// assert_eq!(v, [1_f32, 1_f32, 1_f32, 2_f32.sqrt()]);
/// ```
#[inline]
pub fn w_mut<'out, T>(out: &'out mut [T; 4]) -> &'out mut [T; 4]
where
    T: Real,
    for<'a, 'b> &'a T: Mul<&'b T, Output = T>,
{
    out[3] = (T::one() - &out[0] * &out[0] - &out[1] * &out[1] - &out[2] * &out[2])
        .abs()
        .sqrt();
    out
}
