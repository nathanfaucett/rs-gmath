use num_traits::{One, Zero};

pub use vec4::new;
pub use vec4::new_one;
pub use vec4::new_zero;

#[inline(always)]
pub fn new_identity<T>() -> [T; 4]
where
    T: One + Zero,
{
    new(T::zero(), T::zero(), T::zero(), T::one())
}
