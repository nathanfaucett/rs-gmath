use num_traits::real::Real;
use num_traits::{FromPrimitive, One, Zero};

pub use vec4::set;
pub use vec4::set_backward;
pub use vec4::set_down;
pub use vec4::set_forward;
pub use vec4::set_left;
pub use vec4::set_one;
pub use vec4::set_right;
pub use vec4::set_up;
pub use vec4::set_zero;

#[inline(always)]
pub fn set_identity<T>(out: &mut [T; 4]) -> &mut [T; 4]
where
    T: One + Zero,
{
    set(out, T::zero(), T::zero(), T::zero(), T::one())
}

/// # Example
/// ```
/// use std::f32::consts::FRAC_2_PI;
/// let mut q = quat::new_identity::<f32>();
/// quat::set_axis_angle(&mut q, &[0_f32, 0_f32, 1_f32], &FRAC_2_PI);
/// assert_eq!(q, [0_f32, 0_f32, 0.3129618_f32, 0.94976574_f32]);
/// ```
#[inline]
pub fn set_axis_angle<'out, T>(out: &'out mut [T; 4], axis: &[T; 3], angle: &T) -> &'out mut [T; 4]
where
    T: Real + FromPrimitive,
{
    let half_angle = *angle / T::from_usize(2).unwrap();
    let s = half_angle.sin();
    let c = half_angle.cos();

    out[0] = axis[0] * s;
    out[1] = axis[1] * s;
    out[2] = axis[2] * s;
    out[3] = c;
    out
}
/// # Example
/// ```
/// let mut q = quat::new_identity::<f32>();
/// quat::set_mat3_values(&mut q, 1_f32, 0_f32, 0_f32, 0_f32, 1_f32, 0_f32, 0_f32, 0_f32, 1_f32);
/// assert_eq!(q, [0_f32, 0_f32, 0_f32, 1_f32]);
/// ```
#[inline]
pub fn set_mat3_values<'out, T>(
    out: &'out mut [T; 4],
    m00: T,
    m01: T,
    m02: T,
    m10: T,
    m11: T,
    m12: T,
    m20: T,
    m21: T,
    m22: T,
) -> &'out mut [T; 4]
where
    T: Real + FromPrimitive,
{
    let trace = m00 + m11 + m22;

    if trace > T::zero() {
        let s = T::from_f32(0.5f32).unwrap() / (trace + T::one()).sqrt();

        out[3] = T::from_f32(0.25f32).unwrap() / s;
        out[0] = (m21 - m12) * s;
        out[1] = (m02 - m20) * s;
        out[2] = (m10 - m01) * s;
    } else if m00 > m11 && m00 > m22 {
        let s = T::from_usize(2).unwrap() * (T::one() + m00 - m11 - m22).sqrt();
        let inv_s = T::one() / s;

        out[3] = (m21 - m12) * inv_s;
        out[0] = s / T::from_isize(4isize).unwrap();
        out[1] = (m01 + m10) * inv_s;
        out[2] = (m02 + m20) * inv_s;
    } else if m11 > m22 {
        let s = T::from_usize(2).unwrap() * (T::one() + m11 - m00 - m22).sqrt();
        let inv_s = T::one() / s;

        out[3] = (m02 - m20) * inv_s;
        out[0] = (m01 + m10) * inv_s;
        out[1] = s / T::from_isize(4isize).unwrap();
        out[2] = (m12 + m21) * inv_s;
    } else {
        let s = T::from_usize(2).unwrap() * (T::one() + m22 - m00 - m11).sqrt();
        let inv_s = T::one() / s;

        out[3] = (m10 - m01) * inv_s;
        out[0] = (m02 + m20) * inv_s;
        out[1] = (m12 + m21) * inv_s;
        out[2] = s / T::from_isize(4isize).unwrap();
    }

    out
}

#[inline]
pub fn set_mat2<'out, T>(out: &'out mut [T; 4], m: &[T; 4]) -> &'out mut [T; 4]
where
    T: Real + FromPrimitive,
{
    set_mat3_values(
        out,
        m[0],
        m[2],
        T::zero(),
        m[1],
        m[3],
        T::zero(),
        T::zero(),
        T::zero(),
        T::one(),
    )
}

#[inline]
pub fn set_mat32<'out, T>(out: &'out mut [T; 4], m: &[T; 6]) -> &'out mut [T; 4]
where
    T: Real + FromPrimitive,
{
    set_mat3_values(
        out,
        m[0],
        m[2],
        T::zero(),
        m[1],
        m[3],
        T::zero(),
        T::zero(),
        T::zero(),
        T::one(),
    )
}

#[inline]
pub fn set_mat3<'out, T>(out: &'out mut [T; 4], m: &[T; 9]) -> &'out mut [T; 4]
where
    T: Real + FromPrimitive,
{
    set_mat3_values(out, m[0], m[3], m[6], m[1], m[4], m[7], m[2], m[5], m[8])
}

#[inline]
pub fn set_mat4<'out, T>(out: &'out mut [T; 4], m: &[T; 16]) -> &'out mut [T; 4]
where
    T: Real + FromPrimitive,
{
    set_mat3_values(out, m[0], m[4], m[8], m[1], m[5], m[9], m[2], m[6], m[10])
}
