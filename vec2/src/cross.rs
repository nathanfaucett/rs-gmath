use core::ops::{Mul, Sub};

/// # Example
/// ```
/// let mut v = [1_f32, 0_f32];
/// assert_eq!(vec2::cross(&mut v, &[0_f32, 1_f32]), 1_f32);
/// ```
#[inline]
pub fn cross<T>(a: &[T; 2], b: &[T; 2]) -> T
where
    T: Sub<T, Output = T>,
    for<'a, 'b> &'a T: Mul<&'b T, Output = T>,
{
    &a[0] * &b[1] - &a[1] * &b[0]
}
