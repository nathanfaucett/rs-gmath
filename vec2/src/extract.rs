use core::ops::Mul;

use num_traits::real::Real;
use num_traits::Zero;
use vec3;

use super::{dot, len, len_values};

/// # Example
/// ```
/// let mut v = vec2::new_zero();
/// vec2::position_mat32(&mut v, &[1_f32, 0_f32, 0_f32, 1_f32, 1_f32, 1_f32]);
/// assert_eq!(v, vec2::new_one());
/// ```
#[inline]
pub fn position_mat32<'out, T>(out: &'out mut [T; 2], m: &[T; 6]) -> &'out mut [T; 2]
where
    T: Clone + Zero,
{
    out[0] = m[4].clone();
    out[1] = m[5].clone();
    out
}
/// # Example
/// ```
/// let mut v = vec2::new_zero();
/// vec2::position_mat4(&mut v,
///     &[1_f32, 0_f32, 0_f32, 0_f32, 0_f32, 1_f32, 0_f32, 0_f32, 0_f32, 0_f32, 1_f32, 0_f32, 1_f32, 1_f32, 1_f32, 1_f32]
/// );
/// assert_eq!(v, vec2::new_one());
/// ```
#[inline]
pub fn position_mat4<'out, T>(out: &'out mut [T; 2], m: &[T; 16]) -> &'out mut [T; 2]
where
    T: Clone,
{
    out[0] = m[12].clone();
    out[1] = m[13].clone();
    out
}
/// # Example
/// ```
/// let mut v = vec2::new_zero();
/// vec2::scale_mat2(&mut v, &[1_f32, 0_f32, 0_f32, 1_f32]);
/// assert_eq!(v, vec2::new_one());
/// ```
#[inline]
pub fn scale_mat2<'out, T>(out: &'out mut [T; 2], m: &[T; 4]) -> &'out mut [T; 2]
where
    T: Real,
    for<'a, 'b> &'a T: Mul<&'b T, Output = T>,
{
    out[0] = len_values(&m[0], &m[2]);
    out[1] = len_values(&m[1], &m[3]);
    out
}
/// # Example
/// ```
/// let mut v = vec2::new_zero();
/// vec2::scale_mat32(&mut v, &[1_f32, 0_f32, 0_f32, 1_f32, 1_f32, 1_f32]);
/// assert_eq!(v, vec2::new_one());
/// ```
#[inline]
pub fn scale_mat32<'out, T>(out: &'out mut [T; 2], m: &[T; 6]) -> &'out mut [T; 2]
where
    T: Real,
    for<'a, 'b> &'a T: Mul<&'b T, Output = T>,
{
    out[0] = len_values(&m[0], &m[2]);
    out[1] = len_values(&m[1], &m[3]);
    out
}
/// # Example
/// ```
/// let mut v = vec2::new_zero();
/// vec2::scale_mat3(&mut v, &[1_f32, 0_f32, 0_f32, 0_f32, 1_f32, 0_f32, 0_f32, 0_f32, 1_f32]);
/// assert_eq!(v, vec2::new_one());
/// ```
#[inline]
pub fn scale_mat3<'out, T>(out: &'out mut [T; 2], m: &[T; 9]) -> &'out mut [T; 2]
where
    T: Real,
    for<'a, 'b> &'a T: Mul<&'b T, Output = T>,
{
    out[0] = vec3::len_values(&m[0], &m[3], &m[6]);
    out[1] = vec3::len_values(&m[1], &m[4], &m[7]);
    out
}
/// # Example
/// ```
/// let mut v = vec2::new_zero();
/// vec2::scale_mat4(&mut v,
///     &[1_f32, 0_f32, 0_f32, 0_f32, 0_f32, 1_f32, 0_f32, 0_f32, 0_f32, 0_f32, 1_f32, 0_f32, 0_f32, 0_f32, 0_f32, 1_f32]
/// );
/// assert_eq!(v, vec2::new_one());
/// ```
#[inline]
pub fn scale_mat4<'out, T>(out: &'out mut [T; 2], m: &[T; 16]) -> &'out mut [T; 2]
where
    T: Real,
    for<'a, 'b> &'a T: Mul<&'b T, Output = T>,
{
    out[0] = vec3::len_values(&m[0], &m[4], &m[8]);
    out[1] = vec3::len_values(&m[1], &m[5], &m[9]);
    out
}
/// # Example
/// ```
/// use std::f32::consts::FRAC_PI_2;
/// let mut v: [f32; 2] = vec2::new_zero();
/// assert_eq!(vec2::angle(&[1_f32, 0_f32], &[0_f32, 1_f32]), FRAC_PI_2);
/// ```
#[inline]
pub fn angle<T>(a: &[T; 2], b: &[T; 2]) -> T
where
    T: Real,
    for<'a, 'b> &'a T: Mul<&'b T, Output = T>,
{
    (dot(a, b) / (len(a) * len(b))).acos()
}
