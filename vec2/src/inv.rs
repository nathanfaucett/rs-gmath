use core::ops::{Mul, MulAssign, Neg};
use num_traits::One;

use super::{smul, smul_mut};

/// # Example
/// ```
/// let mut v = vec2::new_zero();
/// vec2::inv(&mut v, &[1; 2]);
/// assert_eq!(v, [-1; 2]);
/// ```
#[inline]
pub fn inv<'out, T>(out: &'out mut [T; 2], b: &[T; 2]) -> &'out mut [T; 2]
where
    T: One + Neg<Output = T>,
    for<'a, 'b> &'a T: Mul<&'b T, Output = T>,
{
    smul(out, b, &-T::one())
}
/// # Example
/// ```
/// let mut v = [1; 2];
/// vec2::inv_mut(&mut v);
/// assert_eq!(v, [-1; 2]);
/// ```
#[inline]
pub fn inv_mut<'out, T>(out: &'out mut [T; 2]) -> &'out mut [T; 2]
where
    T: One + Neg<Output = T>,
    for<'a> T: MulAssign<&'a T>,
{
    smul_mut(out, &-T::one())
}
