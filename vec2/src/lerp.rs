use core::ops::{AddAssign, MulAssign, Sub};

use cast_trait::Cast;

use super::{add_mut, smul_mut, sub};

/// # Example
/// ```
/// let mut v = vec2::new_zero();
/// vec2::lerp(&mut v, &vec2::new_zero(), &[100; 2], &0.5);
/// assert_eq!(v, [50; 2]);
/// ```
#[inline]
pub fn lerp<'out, T, F>(out: &'out mut [T; 2], a: &[T; 2], b: &[T; 2], t: &F) -> &'out mut [T; 2]
where
    T: Clone + Cast<F>,
    F: Clone + Cast<T>,
    for<'a> T: AddAssign<&'a T>,
    for<'a> F: MulAssign<&'a F>,
    for<'a, 'b> &'a T: Sub<&'b T, Output = T>,
{
    let mut tmp = out.clone();
    sub(&mut tmp, b, a);

    let mut tmp_f: [F; 2] = tmp.cast();
    smul_mut(&mut tmp_f, t);

    tmp = tmp_f.cast();
    add_mut(out, &tmp);

    out
}
