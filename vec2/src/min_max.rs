/// # Example
/// ```
/// let mut v = vec2::new_zero::<f32>();
/// vec2::min(&mut v, &vec2::new_zero(), &vec2::new_one());
/// assert_eq!(v, vec2::new_zero());
/// ```
#[inline]
pub fn min<'out, T>(out: &'out mut [T; 2], a: &[T; 2], b: &[T; 2]) -> &'out mut [T; 2]
where
    T: Clone + PartialOrd,
{
    out[0] = if &b[0] < &a[0] {
        b[0].clone()
    } else {
        a[0].clone()
    };
    out[1] = if &b[1] < &a[1] {
        b[1].clone()
    } else {
        a[1].clone()
    };
    out
}
/// # Example
/// ```
/// let mut v = vec2::new_zero::<f32>();
/// vec2::max(&mut v, &vec2::new_zero(), &vec2::new_one());
/// assert_eq!(v, vec2::new_one());
/// ```
#[inline]
pub fn max<'out, T>(out: &'out mut [T; 2], a: &[T; 2], b: &[T; 2]) -> &'out mut [T; 2]
where
    T: Clone + PartialOrd,
{
    out[0] = if &b[0] > &a[0] {
        b[0].clone()
    } else {
        a[0].clone()
    };
    out[1] = if &b[1] > &a[1] {
        b[1].clone()
    } else {
        a[1].clone()
    };
    out
}
/// # Example
/// ```
/// let mut v = [0.5_f32; 2];
/// vec2::clamp(&mut v, &vec2::new_zero(), &vec2::new_one());
/// assert_eq!(v, [0.5_f32; 2]);
///
/// let mut v = [-0.5_f32; 2];
/// vec2::clamp(&mut v, &vec2::new_zero(), &vec2::new_one());
/// assert_eq!(v, vec2::new_zero());
///
/// let mut v = [1.5_f32; 2];
/// vec2::clamp(&mut v, &vec2::new_zero(), &vec2::new_one());
/// assert_eq!(v, vec2::new_one());
/// ```
#[inline]
pub fn clamp<'out, T>(out: &'out mut [T; 2], a: &[T; 2], b: &[T; 2]) -> &'out mut [T; 2]
where
    T: Clone + PartialOrd,
{
    let mut tmp = out.clone();
    max(out, &tmp, a);
    tmp = out.clone();
    min(out, &tmp, b)
}
