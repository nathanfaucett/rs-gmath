use core::ops::Neg;
use num_traits::{One, Zero};

#[inline(always)]
pub fn new<T>(x: T, y: T) -> [T; 2] {
    [x, y]
}

#[inline(always)]
pub fn from_vec3<T>(v: &[T; 3]) -> [T; 2]
where
    T: Clone,
{
    new(v[0].clone(), v[1].clone())
}

#[inline(always)]
pub fn from_vec4<T>(v: &[T; 4]) -> [T; 2]
where
    T: Clone,
{
    new(v[0].clone(), v[1].clone())
}

#[inline(always)]
pub fn new_one<T>() -> [T; 2]
where
    T: One,
{
    new(T::one(), T::one())
}

#[inline(always)]
pub fn new_zero<T>() -> [T; 2]
where
    T: Zero,
{
    new(T::zero(), T::zero())
}

#[inline(always)]
pub fn new_up<T>() -> [T; 2]
where
    T: One + Zero,
{
    new(T::zero(), T::one())
}

#[inline(always)]
pub fn new_down<T>() -> [T; 2]
where
    T: Neg<Output = T> + One + Zero,
{
    new(T::zero(), -T::one())
}

#[inline(always)]
pub fn new_left<T>() -> [T; 2]
where
    T: Neg<Output = T> + One + Zero,
{
    new(-T::one(), T::zero())
}

#[inline(always)]
pub fn new_right<T>() -> [T; 2]
where
    T: One + Zero,
{
    new(T::one(), T::zero())
}
