use core::ops::{Div, Mul};
use num_traits::real::Real;

use super::{len, sdiv};

/// # Example
/// ```
/// let mut v = vec2::new_zero::<f32>();
/// assert_eq!(vec2::norm(&mut v, &vec2::new_one()), 2_f32.sqrt());
/// assert_eq!(v, [1_f32 / 2_f32.sqrt(); 2]);
/// ```
#[inline]
pub fn norm<'out, T>(out: &'out mut [T; 2], b: &[T; 2]) -> T
where
    T: Real,
    for<'a, 'b> &'a T: Mul<&'b T, Output = T> + Div<&'b T, Output = T>,
{
    let s = len(b);
    sdiv(out, b, &s);
    s
}

/// # Example
/// ```
/// let mut v = vec2::new_one::<f32>();
/// assert_eq!(vec2::norm_mut(&mut v), 2_f32.sqrt());
/// assert_eq!(v, [1_f32 / 2_f32.sqrt(); 2]);
/// ```
#[inline]
pub fn norm_mut<'out, T>(out: &'out mut [T; 2]) -> T
where
    T: Real,
    for<'a, 'b> &'a T: Mul<&'b T, Output = T> + Div<&'b T, Output = T>,
{
    let tmp = out.clone();
    norm(out, &tmp)
}
