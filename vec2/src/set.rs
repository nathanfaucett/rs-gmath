use core::ops::Neg;
use num_traits::{One, Zero};

/// # Example
/// ```
/// let mut v = vec2::new_zero();
/// vec2::set(&mut v, 1, 1);
/// assert_eq!(v, [1; 2]);
/// ```
#[inline]
pub fn set<T>(out: &mut [T; 2], x: T, y: T) -> &mut [T; 2] {
    out[0] = x;
    out[1] = y;
    out
}

#[inline(always)]
pub fn set_zero<T>(out: &mut [T; 2]) -> &mut [T; 2]
where
    T: Zero,
{
    set(out, T::zero(), T::zero())
}

#[inline(always)]
pub fn set_one<T>(out: &mut [T; 2]) -> &mut [T; 2]
where
    T: One,
{
    set(out, T::one(), T::one())
}

#[inline(always)]
pub fn set_up<T>(out: &mut [T; 2]) -> &mut [T; 2]
where
    T: One + Zero,
{
    set(out, T::zero(), T::one())
}

#[inline(always)]
pub fn set_down<T>(out: &mut [T; 2]) -> &mut [T; 2]
where
    T: Neg<Output = T> + One + Zero,
{
    set(out, T::zero(), -T::one())
}

#[inline(always)]
pub fn set_left<T>(out: &mut [T; 2]) -> &mut [T; 2]
where
    T: Neg<Output = T> + One + Zero,
{
    set(out, -T::one(), T::zero())
}

#[inline(always)]
pub fn set_right<T>(out: &mut [T; 2]) -> &mut [T; 2]
where
    T: One + Zero,
{
    set(out, T::one(), T::zero())
}

#[inline]
pub fn set_vec3<'out, T>(out: &'out mut [T; 2], v: &[T; 3]) -> &'out mut [T; 2]
where
    T: Clone,
{
    set(out, v[0].clone(), v[1].clone())
}

#[inline]
pub fn set_vec4<'out, T>(out: &'out mut [T; 2], v: &[T; 4]) -> &'out mut [T; 2]
where
    T: Clone,
{
    set(out, v[0].clone(), v[1].clone())
}
