use core::ops::{Add, Div, Mul};

use num_traits::real::Real;
use num_traits::{One, Zero};

/// # Example
/// ```
/// use std::f32::consts::FRAC_PI_2;
/// let mut v = vec2::new_zero();
/// vec2::transform_angle(&mut v, &[0_f32, 1_f32], &FRAC_PI_2);
/// let c = FRAC_PI_2.cos();
/// let s = FRAC_PI_2.sin();
/// assert_eq!(v, [0_f32 * c - 1_f32 * s, 0_f32 * s + 1_f32 * c]);
/// ```
#[inline]
pub fn transform_angle<'out, T>(out: &'out mut [T; 2], v: &[T; 2], a: &T) -> &'out mut [T; 2]
where
    T: Real,
    for<'a, 'b> &'a T: Mul<&'b T, Output = T>,
{
    let c = a.cos();
    let s = a.sin();

    out[0] = &v[0] * &c - &v[1] * &s;
    out[1] = &v[0] * &s + &v[1] * &c;
    out
}
/// # Example
/// ```
/// use std::f32::consts::FRAC_PI_2;
/// let mut v = [0_f32, 1_f32];
/// vec2::transform_angle_mut(&mut v, &FRAC_PI_2);
/// let c = FRAC_PI_2.cos();
/// let s = FRAC_PI_2.sin();
/// assert_eq!(v, [0_f32 * c - 1_f32 * s, 0_f32 * s + 1_f32 * c]);
/// ```
#[inline]
pub fn transform_angle_mut<'out, T>(out: &'out mut [T; 2], a: &T) -> &'out mut [T; 2]
where
    T: Real,
    for<'a, 'b> &'a T: Mul<&'b T, Output = T>,
{
    let tmp = out.clone();
    transform_angle(out, &tmp, a)
}
/// # Example
/// ```
/// let mut v = vec2::new_zero();
/// vec2::transform_mat2(&mut v, &vec2::new_one(), &[1_f32, 0_f32, 0_f32, 1_f32]);
/// assert_eq!(v, vec2::new_one());
/// ```
#[inline]
pub fn transform_mat2<'out, T>(out: &'out mut [T; 2], v: &[T; 2], m: &[T; 4]) -> &'out mut [T; 2]
where
    T: Clone + Add<T, Output = T>,
    for<'a, 'b> &'a T: Mul<&'b T, Output = T>,
{
    out[0] = &v[0] * &m[0] + &v[1] * &m[2];
    out[1] = &v[0] * &m[1] + &v[1] * &m[3];
    out
}
/// # Example
/// ```
/// let mut v = vec2::new_one();
/// vec2::transform_mat2_mut(&mut v, &[1_f32, 0_f32, 0_f32, 1_f32]);
/// assert_eq!(v, vec2::new_one());
/// ```
#[inline]
pub fn transform_mat2_mut<'out, T>(out: &'out mut [T; 2], m: &[T; 4]) -> &'out mut [T; 2]
where
    T: Clone + Add<T, Output = T>,
    for<'a, 'b> &'a T: Mul<&'b T, Output = T>,
{
    let tmp = out.clone();
    transform_mat2(out, &tmp, m)
}
/// # Example
/// ```
/// let mut v = vec2::new_zero();
/// vec2::transform_mat3(&mut v, &vec2::new_one(),
///     &[1_f32, 0_f32, 0_f32, 0_f32, 1_f32, 0_f32, 0_f32, 0_f32, 1_f32],
/// );
/// assert_eq!(v, vec2::new_one());
/// ```
#[inline]
pub fn transform_mat3<'out, T>(out: &'out mut [T; 2], v: &[T; 2], m: &[T; 9]) -> &'out mut [T; 2]
where
    T: Add<T, Output = T>,
    for<'a, 'b> &'a T: Mul<&'b T, Output = T>,
{
    out[0] = &v[0] * &m[0] + &v[1] * &m[3];
    out[1] = &v[0] * &m[1] + &v[1] * &m[4];
    out
}
/// # Example
/// ```
/// let mut v = vec2::new_one();
/// vec2::transform_mat3_mut(&mut v,
///     &[1_f32, 0_f32, 0_f32, 0_f32, 1_f32, 0_f32, 0_f32, 0_f32, 1_f32],
/// );
/// assert_eq!(v, vec2::new_one());
/// ```
#[inline]
pub fn transform_mat3_mut<'out, T>(out: &'out mut [T; 2], m: &[T; 9]) -> &'out mut [T; 2]
where
    T: Clone + Add<T, Output = T>,
    for<'a, 'b> &'a T: Mul<&'b T, Output = T>,
{
    let tmp = out.clone();
    transform_mat3(out, &tmp, m)
}
/// # Example
/// ```
/// let mut v = vec2::new_zero();
/// vec2::transform_mat4(&mut v, &vec2::new_one(),
///     &[1_f32, 0_f32, 0_f32, 0_f32, 0_f32, 1_f32, 0_f32, 0_f32, 0_f32, 0_f32, 1_f32, 0_f32, 0_f32, 0_f32, 0_f32, 1_f32]
/// );
/// assert_eq!(v, vec2::new_one());
/// ```
#[inline]
pub fn transform_mat4<'out, T>(out: &'out mut [T; 2], v: &[T; 2], m: &[T; 16]) -> &'out mut [T; 2]
where
    T: Clone + Add<T, Output = T>,
    for<'a, 'b> &'a T: Mul<&'b T, Output = T>,
{
    out[0] = &v[0] * &m[0] + &v[1] * &m[4] + m[12].clone();
    out[1] = &v[0] * &m[1] + &v[1] * &m[5] + m[13].clone();
    out
}
/// # Example
/// ```
/// let mut v = vec2::new_one();
/// vec2::transform_mat4_mut(&mut v,
///     &[1_f32, 0_f32, 0_f32, 0_f32, 0_f32, 1_f32, 0_f32, 0_f32, 0_f32, 0_f32, 1_f32, 0_f32, 0_f32, 0_f32, 0_f32, 1_f32]
/// );
/// assert_eq!(v, vec2::new_one());
/// ```
#[inline]
pub fn transform_mat4_mut<'out, T>(out: &'out mut [T; 2], m: &[T; 16]) -> &'out mut [T; 2]
where
    T: Clone + Add<T, Output = T>,
    for<'a, 'b> &'a T: Mul<&'b T, Output = T>,
{
    let tmp = out.clone();
    transform_mat4(out, &tmp, m)
}
/// # Example
/// ```
/// let mut v = vec2::new_zero();
/// vec2::transform_mat4_rotation(&mut v, &vec2::new_one(),
///     &[1_f32, 0_f32, 0_f32, 0_f32, 0_f32, 1_f32, 0_f32, 0_f32, 0_f32, 0_f32, 1_f32, 0_f32, 0_f32, 0_f32, 0_f32, 1_f32]
/// );
/// assert_eq!(v, vec2::new_one());
/// ```
#[inline]
pub fn transform_mat4_rotation<'out, T>(
    out: &'out mut [T; 2],
    v: &[T; 2],
    m: &[T; 16],
) -> &'out mut [T; 2]
where
    T: Clone + Add<T, Output = T>,
    for<'a, 'b> &'a T: Mul<&'b T, Output = T>,
{
    out[0] = &v[0] * &m[0] + &v[1] * &m[4];
    out[1] = &v[0] * &m[1] + &v[1] * &m[5];
    out
}
/// # Example
/// ```
/// let mut v = vec2::new_one();
/// vec2::transform_mat4_rotation_mut(&mut v,
///     &[1_f32, 0_f32, 0_f32, 0_f32, 0_f32, 1_f32, 0_f32, 0_f32, 0_f32, 0_f32, 1_f32, 0_f32, 0_f32, 0_f32, 0_f32, 1_f32]
/// );
/// assert_eq!(v, vec2::new_one());
/// ```
#[inline]
pub fn transform_mat4_rotation_mut<'out, T>(out: &'out mut [T; 2], m: &[T; 16]) -> &'out mut [T; 2]
where
    T: Clone + Add<T, Output = T>,
    for<'a, 'b> &'a T: Mul<&'b T, Output = T>,
{
    let tmp = out.clone();
    transform_mat4(out, &tmp, m)
}
/// # Example
/// ```
/// let mut v = vec2::new_zero();
/// vec2::transform_mat4_projection(&mut v, &vec2::new_one(),
///     &[1_f32, 0_f32, 0_f32, 0_f32, 0_f32, 1_f32, 0_f32, 0_f32, 0_f32, 0_f32, 1_f32, 0_f32, 0_f32, 0_f32, 0_f32, 1_f32]
/// );
/// assert_eq!(v, vec2::new_one());
/// ```
#[inline]
pub fn transform_mat4_projection<'out, T>(
    out: &'out mut [T; 2],
    v: &[T; 2],
    m: &[T; 16],
) -> &'out mut [T; 2]
where
    T: Clone + One + Zero + Add<T, Output = T>,
    for<'a, 'b> &'a T: Div<&'b T, Output = T> + Mul<&'b T, Output = T>,
{
    let d = &v[0] * &m[3] + &v[1] * &m[7] + m[15].clone();
    let inv_d = if d.is_zero() { d } else { &T::one() / &d };

    out[0] = &(&v[0] * &m[0] + &v[1] * &m[4] + m[12].clone()) * &inv_d;
    out[1] = &(&v[0] * &m[1] + &v[1] * &m[5] + m[13].clone()) * &inv_d;
    out
}
/// # Example
/// ```
/// let mut v = vec2::new_one();
/// vec2::transform_mat4_projection_mut(&mut v,
///     &[1_f32, 0_f32, 0_f32, 0_f32, 0_f32, 1_f32, 0_f32, 0_f32, 0_f32, 0_f32, 1_f32, 0_f32, 0_f32, 0_f32, 0_f32, 1_f32]
/// );
/// assert_eq!(v, vec2::new_one());
/// ```
#[inline]
pub fn transform_mat4_projection_mut<'out, T>(
    out: &'out mut [T; 2],
    m: &[T; 16],
) -> &'out mut [T; 2]
where
    T: Clone + One + Zero + Add<T, Output = T>,
    for<'a, 'b> &'a T: Div<&'b T, Output = T> + Mul<&'b T, Output = T>,
{
    let tmp = out.clone();
    transform_mat4_projection(out, &tmp, m)
}
