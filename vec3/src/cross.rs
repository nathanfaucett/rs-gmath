use core::ops::{Mul, Sub};

/// # Example
/// ```
/// let mut v = vec3::new_zero();
/// vec3::cross(&mut v, &[1, 0, 0], &[0, 1, 0]);
/// assert_eq!(v, [0, 0, 1]);
/// ```
#[inline]
pub fn cross<'out, T>(out: &'out mut [T; 3], a: &[T; 3], b: &[T; 3]) -> &'out mut [T; 3]
where
    T: Sub<T, Output = T>,
    for<'a, 'b> &'a T: Mul<&'b T, Output = T>,
{
    out[0] = &a[1] * &b[2] - &a[2] * &b[1];
    out[1] = &a[2] * &b[0] - &a[0] * &b[2];
    out[2] = &a[0] * &b[1] - &a[1] * &b[0];
    out
}
/// # Example
/// ```
/// let mut v = [1, 0, 0];
/// vec3::cross_mut(&mut v, &[0, 1, 0]);
/// assert_eq!(v, [0, 0, 1]);
/// ```
#[inline]
pub fn cross_mut<'out, T>(a: &'out mut [T; 3], b: &[T; 3]) -> &'out mut [T; 3]
where
    T: Clone + Sub<T, Output = T>,
    for<'a, 'b> &'a T: Mul<&'b T, Output = T>,
{
    let tmp = a.clone();
    cross(a, &tmp.clone(), b)
}
