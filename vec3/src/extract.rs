use core::ops::Mul;

use num_traits::real::Real;
use num_traits::Zero;

use super::len_values;

/// # Example
/// ```
/// let mut v = vec3::new_one();
/// vec3::position_mat32(&mut v, &[1_f32, 0_f32, 0_f32, 1_f32, 1_f32, 1_f32]);
/// assert_eq!(v, [1_f32, 1_f32, 0_f32]);
/// ```
#[inline]
pub fn position_mat32<'out, T>(out: &'out mut [T; 3], m: &[T; 6]) -> &'out mut [T; 3]
where
    T: Clone + Zero,
{
    out[0] = m[4].clone();
    out[1] = m[5].clone();
    out[2] = T::zero();
    out
}
/// # Example
/// ```
/// let mut v = vec3::new_one();
/// vec3::position_mat4(&mut v,
///     &[1_f32, 0_f32, 0_f32, 0_f32, 0_f32, 1_f32, 0_f32, 0_f32, 0_f32, 0_f32, 1_f32, 0_f32, 1_f32, 1_f32, 1_f32, 1_f32]
/// );
/// assert_eq!(v, [1_f32, 1_f32, 1_f32]);
/// ```
#[inline]
pub fn position_mat4<'out, T>(out: &'out mut [T; 3], m: &[T; 16]) -> &'out mut [T; 3]
where
    T: Clone,
{
    out[0] = m[12].clone();
    out[1] = m[13].clone();
    out[2] = m[14].clone();
    out
}
/// # Example
/// ```
/// let mut v = vec3::new_one();
/// vec3::scale_mat2(&mut v, &[1_f32, 0_f32, 0_f32, 1_f32]);
/// assert_eq!(v, [1_f32, 1_f32, 1_f32]);
/// ```
#[inline]
pub fn scale_mat2<'out, T>(out: &'out mut [T; 3], m: &[T; 4]) -> &'out mut [T; 3]
where
    T: Real,
    for<'a, 'b> &'a T: Mul<&'b T, Output = T>,
{
    out[0] = len_values(&m[0], &m[2], &T::zero());
    out[1] = len_values(&m[1], &m[3], &T::zero());
    out[2] = T::one();
    out
}
/// # Example
/// ```
/// let mut v = vec3::new_one();
/// vec3::scale_mat32(&mut v, &[1_f32, 0_f32, 0_f32, 1_f32, 1_f32, 1_f32]);
/// assert_eq!(v, [1_f32, 1_f32, 1_f32]);
/// ```
#[inline]
pub fn scale_mat32<'out, T>(out: &'out mut [T; 3], m: &[T; 6]) -> &'out mut [T; 3]
where
    T: Real,
    for<'a, 'b> &'a T: Mul<&'b T, Output = T>,
{
    out[0] = len_values(&m[0], &m[2], &T::zero());
    out[1] = len_values(&m[1], &m[3], &T::zero());
    out[2] = T::one();
    out
}
/// # Example
/// ```
/// let mut v = vec3::new_one();
/// vec3::scale_mat3(&mut v, &[1_f32, 0_f32, 0_f32, 0_f32, 1_f32, 0_f32, 0_f32, 0_f32, 1_f32]);
/// assert_eq!(v, [1_f32, 1_f32, 1_f32]);
/// ```
#[inline]
pub fn scale_mat3<'out, T>(out: &'out mut [T; 3], m: &[T; 9]) -> &'out mut [T; 3]
where
    T: Real,
    for<'a, 'b> &'a T: Mul<&'b T, Output = T>,
{
    out[0] = len_values(&m[0], &m[3], &m[6]);
    out[1] = len_values(&m[1], &m[4], &m[7]);
    out[2] = len_values(&m[2], &m[5], &m[8]);
    out
}
/// # Example
/// ```
/// let mut v = vec3::new_one();
/// vec3::scale_mat4(&mut v,
///     &[1_f32, 0_f32, 0_f32, 0_f32, 0_f32, 1_f32, 0_f32, 0_f32, 0_f32, 0_f32, 1_f32, 0_f32, 0_f32, 0_f32, 0_f32, 1_f32]
/// );
/// assert_eq!(v, [1_f32, 1_f32, 1_f32]);
/// ```
#[inline]
pub fn scale_mat4<'out, T>(out: &'out mut [T; 3], m: &[T; 16]) -> &'out mut [T; 3]
where
    T: Real,
    for<'a, 'b> &'a T: Mul<&'b T, Output = T>,
{
    out[0] = len_values(&m[0], &m[4], &m[8]);
    out[1] = len_values(&m[1], &m[5], &m[9]);
    out[2] = len_values(&m[2], &m[6], &m[10]);
    out
}
