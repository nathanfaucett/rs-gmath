use core::ops::{Mul, MulAssign, Neg};
use num_traits::One;

use super::{smul, smul_mut};

/// # Example
/// ```
/// let mut v = [1; 3];
/// vec3::inv(&mut v, &vec3::new_one());
/// assert_eq!(v, [-1; 3]);
/// ```
#[inline]
pub fn inv<'out, T>(out: &'out mut [T; 3], b: &[T; 3]) -> &'out mut [T; 3]
where
    T: One + Neg<Output = T>,
    for<'a, 'b> &'a T: Mul<&'b T, Output = T>,
{
    smul(out, b, &-T::one())
}
/// # Example
/// ```
/// let mut v = [1; 3];
/// vec3::inv_mut(&mut v);
/// assert_eq!(v, [-1; 3]);
/// ```
#[inline]
pub fn inv_mut<'out, T>(out: &'out mut [T; 3]) -> &'out mut [T; 3]
where
    T: One + Neg<Output = T>,
    for<'a> T: MulAssign<&'a T>,
{
    smul_mut(out, &-T::one())
}
