use core::ops::{Add, Mul};
use num_traits::real::Real;
/// # Example
/// ```
/// assert_eq!(vec3::dot_values(&1_f32, &1_f32, &1_f32, &1_f32, &1_f32, &1_f32), 3_f32);
/// ```
#[inline(always)]
pub fn dot_values<T>(ax: &T, ay: &T, az: &T, bx: &T, by: &T, bz: &T) -> T
where
    T: Add<T, Output = T>,
    for<'a, 'b> &'a T: Mul<&'b T, Output = T>,
{
    ax * bx + ay * by + az * bz
}
/// # Example
/// ```
/// assert_eq!(vec3::len_values_sq(&1_f32, &1_f32, &1_f32), 3_f32);
/// ```
#[inline]
pub fn len_values_sq<T>(x: &T, y: &T, z: &T) -> T
where
    T: Add<T, Output = T>,
    for<'a, 'b> &'a T: Mul<&'b T, Output = T>,
{
    dot_values(x, y, z, x, y, z)
}
/// # Example
/// ```
/// assert_eq!(vec3::inv_len_values(&1_f32, &1_f32, &1_f32), 1_f32 / 3_f32.sqrt());
/// ```
#[inline]
pub fn inv_len_values<T>(x: &T, y: &T, z: &T) -> T
where
    T: Real,
    for<'a, 'b> &'a T: Mul<&'b T, Output = T>,
{
    let len_sq = len_values_sq(x, y, z);

    if len_sq.is_zero() {
        T::zero()
    } else {
        T::one() / len_sq.sqrt()
    }
}
/// # Example
/// ```
/// assert_eq!(vec3::len_values(&1_f32, &1_f32, &1_f32), 3_f32.sqrt());
/// ```
#[inline]
pub fn len_values<T>(x: &T, y: &T, z: &T) -> T
where
    T: Real,
    for<'a, 'b> &'a T: Mul<&'b T, Output = T>,
{
    let len_sq = len_values_sq(x, y, z);

    if len_sq.is_zero() {
        T::zero()
    } else {
        len_sq.sqrt()
    }
}
/// # Example
/// ```
/// assert_eq!(vec3::len_sq::<f32>(&vec3::new_one()), 3_f32);
/// ```
#[inline(always)]
pub fn len_sq<T>(v: &[T; 3]) -> T
where
    T: Add<T, Output = T>,
    for<'a, 'b> &'a T: Mul<&'b T, Output = T>,
{
    len_values_sq(&v[0], &v[1], &v[2])
}
/// # Example
/// ```
/// assert_eq!(vec3::dot::<f32>(&vec3::new_one(), &vec3::new_one()), 3_f32);
/// ```
#[inline(always)]
pub fn dot<T>(a: &[T; 3], b: &[T; 3]) -> T
where
    T: Add<T, Output = T>,
    for<'a, 'b> &'a T: Mul<&'b T, Output = T>,
{
    dot_values(&a[0], &a[1], &a[2], &b[0], &b[1], &b[2])
}
/// # Example
/// ```
/// assert_eq!(vec3::len::<f32>(&vec3::new_one()), 3_f32.sqrt());
/// ```
#[inline(always)]
pub fn len<T>(v: &[T; 3]) -> T
where
    T: Real,
    for<'a, 'b> &'a T: Mul<&'b T, Output = T>,
{
    len_values(&v[0], &v[1], &v[2])
}
/// # Example
/// ```
/// assert_eq!(vec3::inv_len::<f32>(&vec3::new_one()), 1_f32 / 3_f32.sqrt());
/// ```
#[inline(always)]
pub fn inv_len<T>(v: &[T; 3]) -> T
where
    T: Real,
    for<'a, 'b> &'a T: Mul<&'b T, Output = T>,
{
    inv_len_values(&v[0], &v[1], &v[2])
}
