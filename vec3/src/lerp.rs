use core::ops::{AddAssign, MulAssign, Sub};

use cast_trait::Cast;

use super::{add_mut, smul_mut, sub};

/// # Example
/// ```
/// let mut v = vec3::new_zero();
/// vec3::lerp(&mut v, &vec3::new_zero(), &[100; 3], &0.5);
/// assert_eq!(v, [50; 3]);
/// ```
#[inline]
pub fn lerp<'out, T, F>(out: &'out mut [T; 3], a: &[T; 3], b: &[T; 3], t: &F) -> &'out mut [T; 3]
where
    T: Clone + Cast<F>,
    F: Clone + Cast<T>,
    for<'a> T: AddAssign<&'a T>,
    for<'a> F: MulAssign<&'a F>,
    for<'a, 'b> &'a T: Sub<&'b T, Output = T>,
{
    let mut tmp = out.clone();
    sub(&mut tmp, b, a);

    let mut tmp_f: [F; 3] = tmp.cast();
    smul_mut(&mut tmp_f, t);

    tmp = tmp_f.cast();
    add_mut(out, &tmp);

    out
}
