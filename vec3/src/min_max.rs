/// # Example
/// ```
/// let mut v = vec3::new_one::<f32>();
/// vec3::min(&mut v, &vec3::new_one(), &vec3::new_one());
/// assert_eq!(v, vec3::new_one());
/// ```
#[inline]
pub fn min<'out, T>(out: &'out mut [T; 3], a: &[T; 3], b: &[T; 3]) -> &'out mut [T; 3]
where
    T: Clone + PartialOrd,
{
    out[0] = if &b[0] < &a[0] {
        b[0].clone()
    } else {
        a[0].clone()
    };
    out[1] = if &b[1] < &a[1] {
        b[1].clone()
    } else {
        a[1].clone()
    };
    out[2] = if &b[2] < &a[2] {
        b[2].clone()
    } else {
        a[2].clone()
    };
    out
}
/// # Example
/// ```
/// let mut v = vec3::new_one::<f32>();
/// vec3::max(&mut v, &vec3::new_one(), &vec3::new_one());
/// assert_eq!(v, vec3::new_one());
/// ```
#[inline]
pub fn max<'out, T>(out: &'out mut [T; 3], a: &[T; 3], b: &[T; 3]) -> &'out mut [T; 3]
where
    T: Clone + PartialOrd,
{
    out[0] = if &b[0] > &a[0] {
        b[0].clone()
    } else {
        a[0].clone()
    };
    out[1] = if &b[1] > &a[1] {
        b[1].clone()
    } else {
        a[1].clone()
    };
    out[2] = if &b[2] > &a[2] {
        b[2].clone()
    } else {
        a[2].clone()
    };
    out
}
/// # Example
/// ```
/// let mut v = [0.5_f32; 3];
/// vec3::clamp(&mut v, &vec3::new_zero(), &vec3::new_one());
/// assert_eq!(v, [0.5_f32; 3]);
///
/// let mut v = [-0.5_f32; 3];
/// vec3::clamp(&mut v, &vec3::new_zero(), &vec3::new_one());
/// assert_eq!(v, vec3::new_zero());
///
/// let mut v = [1.5_f32; 3];
/// vec3::clamp(&mut v, &vec3::new_zero(), &vec3::new_one());
/// assert_eq!(v, vec3::new_one());
/// ```
#[inline]
pub fn clamp<'out, T>(out: &'out mut [T; 3], a: &[T; 3], b: &[T; 3]) -> &'out mut [T; 3]
where
    T: Clone + PartialOrd,
{
    let mut tmp = out.clone();
    max(out, &tmp, a);
    tmp = out.clone();
    min(out, &tmp, b)
}
