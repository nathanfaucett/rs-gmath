use core::ops::Neg;
use num_traits::{One, Zero};

#[inline(always)]
pub fn new<T>(x: T, y: T, z: T) -> [T; 3] {
    [x, y, z]
}

#[inline(always)]
pub fn from_vec2<T>(v: &[T; 2]) -> [T; 3]
where
    T: Zero + Clone,
{
    new(v[0].clone(), v[1].clone(), T::zero())
}

#[inline(always)]
pub fn from_vec4<T>(v: &[T; 4]) -> [T; 3]
where
    T: Clone,
{
    new(v[0].clone(), v[1].clone(), v[2].clone())
}

#[inline(always)]
pub fn new_one<T>() -> [T; 3]
where
    T: One,
{
    new(T::one(), T::one(), T::one())
}

#[inline(always)]
pub fn new_zero<T>() -> [T; 3]
where
    T: Zero,
{
    new(T::zero(), T::zero(), T::zero())
}

#[inline(always)]
pub fn new_up<T>() -> [T; 3]
where
    T: One + Zero,
{
    new(T::zero(), T::zero(), T::one())
}

#[inline(always)]
pub fn new_down<T>() -> [T; 3]
where
    T: Neg<Output = T> + One + Zero,
{
    new(T::zero(), T::zero(), -T::one())
}

#[inline(always)]
pub fn new_left<T>() -> [T; 3]
where
    T: Neg<Output = T> + One + Zero,
{
    new(-T::one(), T::zero(), T::zero())
}

#[inline(always)]
pub fn new_right<T>() -> [T; 3]
where
    T: One + Zero,
{
    new(T::one(), T::zero(), T::zero())
}

#[inline(always)]
pub fn new_forward<T>() -> [T; 3]
where
    T: One + Zero,
{
    new(T::zero(), T::one(), T::zero())
}

#[inline(always)]
pub fn new_backward<T>() -> [T; 3]
where
    T: Neg<Output = T> + One + Zero,
{
    new(T::zero(), -T::one(), T::zero())
}
