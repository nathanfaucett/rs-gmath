use core::ops::{Add, Div, Mul, Neg, Sub};

use num_traits::{One, Zero};

/// # Example
/// ```
/// let mut v = vec3::new_zero();
/// vec3::transform_mat2(&mut v, &vec3::new_one(), &[1_f32, 0_f32, 0_f32, 1_f32]);
/// assert_eq!(v, vec3::new_one());
/// ```
#[inline]
pub fn transform_mat2<'out, T>(out: &'out mut [T; 3], v: &[T; 3], m: &[T; 4]) -> &'out mut [T; 3]
where
    T: Clone + Add<T, Output = T>,
    for<'a, 'b> &'a T: Mul<&'b T, Output = T>,
{
    out[0] = &v[0] * &m[0] + &v[1] * &m[2];
    out[1] = &v[0] * &m[1] + &v[1] * &m[3];
    out[2] = v[2].clone();
    out
}
/// # Example
/// ```
/// let mut v = vec3::new_one();
/// vec3::transform_mat2_mut(&mut v, &[1_f32, 0_f32, 0_f32, 1_f32]);
/// assert_eq!(v, vec3::new_one());
/// ```
#[inline]
pub fn transform_mat2_mut<'out, T>(out: &'out mut [T; 3], m: &[T; 4]) -> &'out mut [T; 3]
where
    T: Clone + Add<T, Output = T>,
    for<'a, 'b> &'a T: Mul<&'b T, Output = T>,
{
    let tmp = out.clone();
    transform_mat2(out, &tmp, m)
}
/// # Example
/// ```
/// let mut v = vec3::new_zero();
/// vec3::transform_mat3(&mut v, &vec3::new_one(),
///     &[1_f32, 0_f32, 0_f32, 0_f32, 1_f32, 0_f32, 0_f32, 0_f32, 1_f32],
/// );
/// assert_eq!(v, vec3::new_one());
/// ```
#[inline]
pub fn transform_mat3<'out, T>(out: &'out mut [T; 3], v: &[T; 3], m: &[T; 9]) -> &'out mut [T; 3]
where
    T: Add<T, Output = T>,
    for<'a, 'b> &'a T: Mul<&'b T, Output = T>,
{
    out[0] = &v[0] * &m[0] + &v[1] * &m[3] + &v[2] * &m[6];
    out[1] = &v[0] * &m[1] + &v[1] * &m[4] + &v[2] * &m[7];
    out[2] = &v[0] * &m[2] + &v[1] * &m[5] + &v[2] * &m[8];
    out
}
/// # Example
/// ```
/// let mut v = vec3::new_one();
/// vec3::transform_mat3_mut(&mut v,
///     &[1_f32, 0_f32, 0_f32, 0_f32, 1_f32, 0_f32, 0_f32, 0_f32, 1_f32],
/// );
/// assert_eq!(v, vec3::new_one());
/// ```
#[inline]
pub fn transform_mat3_mut<'out, T>(out: &'out mut [T; 3], m: &[T; 9]) -> &'out mut [T; 3]
where
    T: Clone + Add<T, Output = T>,
    for<'a, 'b> &'a T: Mul<&'b T, Output = T>,
{
    let tmp = out.clone();
    transform_mat3(out, &tmp, m)
}
/// # Example
/// ```
/// let mut v = vec3::new_zero();
/// vec3::transform_mat4(&mut v, &vec3::new_one(),
///     &[1_f32, 0_f32, 0_f32, 0_f32, 0_f32, 1_f32, 0_f32, 0_f32, 0_f32, 0_f32, 1_f32, 0_f32, 0_f32, 0_f32, 0_f32, 1_f32]
/// );
/// assert_eq!(v, vec3::new_one());
/// ```
#[inline]
pub fn transform_mat4<'out, T>(out: &'out mut [T; 3], v: &[T; 3], m: &[T; 16]) -> &'out mut [T; 3]
where
    T: Clone + Add<T, Output = T>,
    for<'a, 'b> &'a T: Mul<&'b T, Output = T>,
{
    out[0] = &v[0] * &m[0] + &v[1] * &m[4] + &v[2] * &m[8] + m[12].clone();
    out[1] = &v[0] * &m[1] + &v[1] * &m[5] + &v[2] * &m[9] + m[13].clone();
    out[2] = &v[0] * &m[2] + &v[1] * &m[6] + &v[2] * &m[10] + m[14].clone();
    out
}
/// # Example
/// ```
/// let mut v = vec3::new_one();
/// vec3::transform_mat4_mut(&mut v,
///     &[1_f32, 0_f32, 0_f32, 0_f32, 0_f32, 1_f32, 0_f32, 0_f32, 0_f32, 0_f32, 1_f32, 0_f32, 0_f32, 0_f32, 0_f32, 1_f32]
/// );
/// assert_eq!(v, vec3::new_one());
/// ```
#[inline]
pub fn transform_mat4_mut<'out, T>(out: &'out mut [T; 3], m: &[T; 16]) -> &'out mut [T; 3]
where
    T: Clone + Add<T, Output = T>,
    for<'a, 'b> &'a T: Mul<&'b T, Output = T>,
{
    let tmp = out.clone();
    transform_mat4(out, &tmp, m)
}
/// # Example
/// ```
/// let mut v = vec3::new_zero();
/// vec3::transform_mat4_rotation(&mut v, &vec3::new_one(),
///     &[1_f32, 0_f32, 0_f32, 0_f32, 0_f32, 1_f32, 0_f32, 0_f32, 0_f32, 0_f32, 1_f32, 0_f32, 0_f32, 0_f32, 0_f32, 1_f32]
/// );
/// assert_eq!(v, vec3::new_one());
/// ```
#[inline]
pub fn transform_mat4_rotation<'out, T>(
    out: &'out mut [T; 3],
    v: &[T; 3],
    m: &[T; 16],
) -> &'out mut [T; 3]
where
    T: Clone + Add<T, Output = T>,
    for<'a, 'b> &'a T: Mul<&'b T, Output = T>,
{
    out[0] = &v[0] * &m[0] + &v[1] * &m[4] + &v[2] * &m[8];
    out[1] = &v[0] * &m[1] + &v[1] * &m[5] + &v[2] * &m[9];
    out[2] = &v[0] * &m[2] + &v[1] * &m[6] + &v[2] * &m[10];
    out
}
/// # Example
/// ```
/// let mut v = vec3::new_one();
/// vec3::transform_mat4_rotation_mut(&mut v,
///     &[1_f32, 0_f32, 0_f32, 0_f32, 0_f32, 1_f32, 0_f32, 0_f32, 0_f32, 0_f32, 1_f32, 0_f32, 0_f32, 0_f32, 0_f32, 1_f32]
/// );
/// assert_eq!(v, vec3::new_one());
/// ```
#[inline]
pub fn transform_mat4_rotation_mut<'out, T>(out: &'out mut [T; 3], m: &[T; 16]) -> &'out mut [T; 3]
where
    T: Clone + Add<T, Output = T>,
    for<'a, 'b> &'a T: Mul<&'b T, Output = T>,
{
    let tmp = out.clone();
    transform_mat4(out, &tmp, m)
}
/// # Example
/// ```
/// let mut v = vec3::new_zero();
/// vec3::transform_mat4_projection(&mut v, &vec3::new_one(),
///     &[1_f32, 0_f32, 0_f32, 0_f32, 0_f32, 1_f32, 0_f32, 0_f32, 0_f32, 0_f32, 1_f32, 0_f32, 0_f32, 0_f32, 0_f32, 1_f32]
/// );
/// assert_eq!(v, vec3::new_one());
/// ```
#[inline]
pub fn transform_mat4_projection<'out, T>(
    out: &'out mut [T; 3],
    v: &[T; 3],
    m: &[T; 16],
) -> &'out mut [T; 3]
where
    T: Clone + One + Zero + Add<T, Output = T>,
    for<'a, 'b> &'a T: Div<&'b T, Output = T> + Mul<&'b T, Output = T>,
{
    let d = &v[0] * &m[3] + &v[1] * &m[7] + &v[2] * &m[11] + m[15].clone();
    let inv_d = if d.is_zero() { d } else { &T::one() / &d };

    out[0] = &(&v[0] * &m[0] + &v[1] * &m[4] + &v[2] * &m[8] + m[12].clone()) * &inv_d;
    out[1] = &(&v[0] * &m[1] + &v[1] * &m[5] + &v[2] * &m[9] + m[13].clone()) * &inv_d;
    out[2] = &(&v[0] * &m[2] + &v[1] * &m[6] + &v[2] * &m[10] + m[14].clone()) * &inv_d;
    out
}
/// # Example
/// ```
/// let mut v = vec3::new_one();
/// vec3::transform_mat4_projection_mut(&mut v,
///     &[1_f32, 0_f32, 0_f32, 0_f32, 0_f32, 1_f32, 0_f32, 0_f32, 0_f32, 0_f32, 1_f32, 0_f32, 0_f32, 0_f32, 0_f32, 1_f32]
/// );
/// assert_eq!(v, vec3::new_one());
/// ```
#[inline]
pub fn transform_mat4_projection_mut<'out, T>(
    out: &'out mut [T; 3],
    m: &[T; 16],
) -> &'out mut [T; 3]
where
    T: Clone + One + Zero + Add<T, Output = T>,
    for<'a, 'b> &'a T: Div<&'b T, Output = T> + Mul<&'b T, Output = T>,
{
    let tmp = out.clone();
    transform_mat4_projection(out, &tmp, m)
}
/// # Example
/// ```
/// let mut v = vec3::new_zero();
/// vec3::transform_quat(&mut v, &vec3::new_one(), &[0_f32, 0_f32, 0_f32, 1_f32]);
/// assert_eq!(v, vec3::new_one());
/// ```
#[inline]
pub fn transform_quat<'out, T>(out: &'out mut [T; 3], v: &[T; 3], q: &[T; 4]) -> &'out mut [T; 3]
where
    T: Add<T, Output = T> + Sub<T, Output = T>,
    for<'a, 'b> &'a T: Neg<Output = T> + Mul<&'b T, Output = T>,
{
    let ix = &q[3] * &v[0] + &q[1] * &v[2] - &q[2] * &v[1];
    let iy = &q[3] * &v[1] + &q[2] * &v[0] - &q[0] * &v[2];
    let iz = &q[3] * &v[2] + &q[0] * &v[1] - &q[1] * &v[0];
    let iw = &-&q[0] * &v[0] - &q[1] * &v[1] - &q[2] * &v[2];

    out[0] = &ix * &q[3] + &iw * &-&q[0] + &iy * &-&q[2] - &iz * &-&q[1];
    out[1] = &iy * &q[3] + &iw * &-&q[1] + &iz * &-&q[0] - &ix * &-&q[2];
    out[2] = &iz * &q[3] + &iw * &-&q[2] + &ix * &-&q[1] - &iy * &-&q[0];
    out
}
/// # Example
/// ```
/// let mut v = vec3::new_one();
/// vec3::transform_quat_mut(&mut v, &[0_f32, 0_f32, 0_f32, 1_f32]);
/// assert_eq!(v, vec3::new_one());
/// ```
#[inline]
pub fn transform_quat_mut<'out, T>(out: &'out mut [T; 3], q: &[T; 4]) -> &'out mut [T; 3]
where
    T: Clone + Add<T, Output = T> + Sub<T, Output = T>,
    for<'a, 'b> &'a T: Neg<Output = T> + Mul<&'b T, Output = T>,
{
    let tmp = out.clone();
    transform_quat(out, &tmp, q)
}
