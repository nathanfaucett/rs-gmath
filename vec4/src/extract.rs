use core::ops::Mul;

use num_traits::real::Real;
use num_traits::{One, Zero};
use vec2;
use vec3;

use super::len_values;

/// # Example
/// ```
/// let mut v = vec4::new_one();
/// vec4::position_mat32(&mut v, &[1_f32, 0_f32, 0_f32, 1_f32, 1_f32, 1_f32]);
/// assert_eq!(v, [1_f32, 1_f32, 0_f32, 1_f32]);
/// ```
#[inline]
pub fn position_mat32<'out, T>(out: &'out mut [T; 4], m: &[T; 6]) -> &'out mut [T; 4]
where
    T: Clone + One + Zero,
{
    out[0] = m[4].clone();
    out[1] = m[5].clone();
    out[2] = T::zero();
    out[3] = T::one();
    out
}
/// # Example
/// ```
/// let mut v = vec4::new_one();
/// vec4::position_mat4(&mut v,
///     &[1_f32, 0_f32, 0_f32, 0_f32, 0_f32, 1_f32, 0_f32, 0_f32, 0_f32, 0_f32, 1_f32, 0_f32, 1_f32, 1_f32, 1_f32, 1_f32]
/// );
/// assert_eq!(v, vec4::new_one());
/// ```
#[inline]
pub fn position_mat4<'out, T>(out: &'out mut [T; 4], m: &[T; 16]) -> &'out mut [T; 4]
where
    T: Clone,
{
    out[0] = m[12].clone();
    out[1] = m[13].clone();
    out[2] = m[14].clone();
    out[3] = m[15].clone();
    out
}
/// # Example
/// ```
/// let mut v = vec4::new_one();
/// vec4::scale_mat2(&mut v, &[1_f32, 0_f32, 0_f32, 1_f32]);
/// assert_eq!(v, vec4::new_one());
/// ```
#[inline]
pub fn scale_mat2<'out, T>(out: &'out mut [T; 4], m: &[T; 4]) -> &'out mut [T; 4]
where
    T: Real,
    for<'a, 'b> &'a T: Mul<&'b T, Output = T>,
{
    out[0] = vec2::len_values(&m[0], &m[2]);
    out[1] = vec2::len_values(&m[1], &m[3]);
    out[2] = T::one();
    out[3] = T::one();
    out
}
/// # Example
/// ```
/// let mut v = vec4::new_one();
/// vec4::scale_mat32(&mut v, &[1_f32, 0_f32, 0_f32, 1_f32, 1_f32, 1_f32]);
/// assert_eq!(v, vec4::new_one());
/// ```
#[inline]
pub fn scale_mat32<'out, T>(out: &'out mut [T; 4], m: &[T; 6]) -> &'out mut [T; 4]
where
    T: Real,
    for<'a, 'b> &'a T: Mul<&'b T, Output = T>,
{
    out[0] = vec2::len_values(&m[0], &m[2]);
    out[1] = vec2::len_values(&m[1], &m[3]);
    out[2] = T::one();
    out[3] = T::one();
    out
}
/// # Example
/// ```
/// let mut v = vec4::new_one();
/// vec4::scale_mat3(&mut v, &[1_f32, 0_f32, 0_f32, 0_f32, 1_f32, 0_f32, 0_f32, 0_f32, 1_f32]);
/// assert_eq!(v, vec4::new_one());
/// ```
#[inline]
pub fn scale_mat3<'out, T>(out: &'out mut [T; 4], m: &[T; 9]) -> &'out mut [T; 4]
where
    T: Real,
    for<'a, 'b> &'a T: Mul<&'b T, Output = T>,
{
    out[0] = vec3::len_values(&m[0], &m[3], &m[6]);
    out[1] = vec3::len_values(&m[1], &m[4], &m[7]);
    out[2] = vec3::len_values(&m[2], &m[5], &m[8]);
    out[3] = T::one();
    out
}
/// # Example
/// ```
/// let mut v = vec4::new_one();
/// vec4::scale_mat4(&mut v,
///     &[1_f32, 0_f32, 0_f32, 0_f32, 0_f32, 1_f32, 0_f32, 0_f32, 0_f32, 0_f32, 1_f32, 0_f32, 0_f32, 0_f32, 0_f32, 1_f32]
/// );
/// assert_eq!(v, vec4::new_one());
/// ```
#[inline]
pub fn scale_mat4<'out, T>(out: &'out mut [T; 4], m: &[T; 16]) -> &'out mut [T; 4]
where
    T: Real,
    for<'a, 'b> &'a T: Mul<&'b T, Output = T>,
{
    out[0] = len_values(&m[0], &m[4], &m[8], &m[12]);
    out[1] = len_values(&m[1], &m[5], &m[9], &m[13]);
    out[2] = len_values(&m[2], &m[6], &m[10], &m[14]);
    out[3] = len_values(&m[3], &m[7], &m[11], &m[15]);
    out
}
