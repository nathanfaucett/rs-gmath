use core::ops::{Mul, MulAssign, Neg};
use num_traits::One;

use super::{smul, smul_mut};

/// # Example
/// ```
/// let mut v = vec4::new_zero::<isize>();
/// vec4::inv(&mut v, &vec4::new_one::<isize>());
/// assert_eq!(v, [-1; 4]);
/// ```
#[inline]
pub fn inv<'out, T>(out: &'out mut [T; 4], b: &[T; 4]) -> &'out mut [T; 4]
where
    T: One + Neg<Output = T>,
    for<'a, 'b> &'a T: Mul<&'b T, Output = T>,
{
    smul(out, b, &-T::one())
}
/// # Example
/// ```
/// let mut v = vec4::new_one::<isize>();
/// vec4::inv_mut(&mut v);
/// assert_eq!(v, [-1; 4]);
/// ```
#[inline]
pub fn inv_mut<'out, T>(out: &'out mut [T; 4]) -> &'out mut [T; 4]
where
    T: One + Neg<Output = T>,
    for<'a> T: MulAssign<&'a T>,
{
    smul_mut(out, &-T::one())
}
