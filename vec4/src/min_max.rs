/// # Example
/// ```
/// let mut v = vec4::new_one();
/// vec4::min(&mut v, &vec4::new_one::<isize>(), &vec4::new_one::<isize>());
/// assert_eq!(v, vec4::new_one());
/// ```
#[inline]
pub fn min<'out, T>(out: &'out mut [T; 4], a: &[T; 4], b: &[T; 4]) -> &'out mut [T; 4]
where
    T: Clone + PartialOrd,
{
    out[0] = if &b[0] < &a[0] {
        b[0].clone()
    } else {
        a[0].clone()
    };
    out[1] = if &b[1] < &a[1] {
        b[1].clone()
    } else {
        a[1].clone()
    };
    out[2] = if &b[2] < &a[2] {
        b[2].clone()
    } else {
        a[2].clone()
    };
    out[3] = if &b[3] < &a[3] {
        b[3].clone()
    } else {
        a[3].clone()
    };
    out
}
/// # Example
/// ```
/// let mut v = vec4::new_one();
/// vec4::max(&mut v, &vec4::new_one::<isize>(), &vec4::new_one::<isize>());
/// assert_eq!(v, vec4::new_one());
/// ```
#[inline]
pub fn max<'out, T>(out: &'out mut [T; 4], a: &[T; 4], b: &[T; 4]) -> &'out mut [T; 4]
where
    T: Clone + PartialOrd,
{
    out[0] = if &b[0] > &a[0] {
        b[0].clone()
    } else {
        a[0].clone()
    };
    out[1] = if &b[1] > &a[1] {
        b[1].clone()
    } else {
        a[1].clone()
    };
    out[2] = if &b[2] > &a[2] {
        b[2].clone()
    } else {
        a[2].clone()
    };
    out[3] = if &b[3] > &a[3] {
        b[3].clone()
    } else {
        a[3].clone()
    };
    out
}
/// # Example
/// ```
/// let mut v = [0.5_f32; 4];
/// vec4::clamp(&mut v, &vec4::new_zero(), &vec4::new_one());
/// assert_eq!(v, [0.5_f32; 4]);
///
/// let mut v = [-0.5_f32; 4];
/// vec4::clamp(&mut v, &vec4::new_zero(), &vec4::new_one());
/// assert_eq!(v, vec4::new_zero());
///
/// let mut v = [1.5_f32; 4];
/// vec4::clamp(&mut v, &vec4::new_zero(), &vec4::new_one());
/// assert_eq!(v, vec4::new_one());
/// ```
#[inline]
pub fn clamp<'out, T>(out: &'out mut [T; 4], a: &[T; 4], b: &[T; 4]) -> &'out mut [T; 4]
where
    T: Clone + PartialOrd,
{
    let mut tmp = out.clone();
    max(out, &tmp, a);
    tmp = out.clone();
    min(out, &tmp, b)
}
