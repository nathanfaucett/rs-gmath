use core::ops::Neg;
use num_traits::{One, Zero};

/// # Example
/// ```
/// let mut v = vec4::new_zero();
/// vec4::set(&mut v, 1, 1, 1, 1);
/// assert_eq!(v, vec4::new_one());
/// ```
#[inline]
pub fn set<T>(out: &mut [T; 4], x: T, y: T, z: T, w: T) -> &mut [T; 4] {
    out[0] = x;
    out[1] = y;
    out[2] = z;
    out[3] = w;
    out
}

#[inline(always)]
pub fn set_zero<T>(out: &mut [T; 4]) -> &mut [T; 4]
where
    T: Zero,
{
    set(out, T::zero(), T::zero(), T::zero(), T::zero())
}

#[inline(always)]
pub fn set_one<T>(out: &mut [T; 4]) -> &mut [T; 4]
where
    T: One,
{
    set(out, T::one(), T::one(), T::one(), T::one())
}

#[inline(always)]
pub fn set_up<T>(out: &mut [T; 4]) -> &mut [T; 4]
where
    T: One + Zero,
{
    set(out, T::zero(), T::zero(), T::one(), T::one())
}

#[inline(always)]
pub fn set_down<T>(out: &mut [T; 4]) -> &mut [T; 4]
where
    T: Neg<Output = T> + One + Zero,
{
    set(out, T::zero(), T::zero(), -T::one(), T::one())
}

#[inline(always)]
pub fn set_left<T>(out: &mut [T; 4]) -> &mut [T; 4]
where
    T: Neg<Output = T> + One + Zero,
{
    set(out, -T::one(), T::zero(), T::zero(), T::one())
}

#[inline(always)]
pub fn set_right<T>(out: &mut [T; 4]) -> &mut [T; 4]
where
    T: One + Zero,
{
    set(out, T::one(), T::zero(), T::zero(), T::one())
}

#[inline(always)]
pub fn set_forward<T>(out: &mut [T; 4]) -> &mut [T; 4]
where
    T: One + Zero,
{
    set(out, T::zero(), T::one(), T::zero(), T::one())
}

#[inline(always)]
pub fn set_backward<T>(out: &mut [T; 4]) -> &mut [T; 4]
where
    T: Neg<Output = T> + One + Zero,
{
    set(out, T::zero(), -T::one(), T::zero(), T::one())
}

#[inline]
pub fn set_vec2<'out, T>(out: &'out mut [T; 4], v: &[T; 2]) -> &'out mut [T; 4]
where
    T: Clone + One + Zero,
{
    set(out, v[0].clone(), v[1].clone(), T::zero(), T::one())
}

#[inline]
pub fn set_vec3<'out, T>(out: &'out mut [T; 4], v: &[T; 3]) -> &'out mut [T; 4]
where
    T: Clone + One,
{
    set(out, v[0].clone(), v[1].clone(), v[2].clone(), T::one())
}
