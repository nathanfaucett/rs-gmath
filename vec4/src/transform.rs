use core::ops::{Add, Div, Mul};

use num_traits::{One, Zero};

/// # Example
/// ```
/// let mut v = vec4::new_zero();
/// vec4::transform_mat2(&mut v, &vec4::new_one(), &[1_f32, 0_f32, 0_f32, 1_f32]);
/// assert_eq!(v, vec4::new_one());
/// ```
#[inline]
pub fn transform_mat2<'out, T>(out: &'out mut [T; 4], v: &[T; 4], m: &[T; 4]) -> &'out mut [T; 4]
where
    T: Clone + Add<T, Output = T>,
    for<'a, 'b> &'a T: Mul<&'b T, Output = T>,
{
    out[0] = &v[0] * &m[0] + &v[1] * &m[2];
    out[1] = &v[0] * &m[1] + &v[1] * &m[3];
    out[2] = v[2].clone();
    out[3] = v[3].clone();
    out
}
/// # Example
/// ```
/// let mut v = vec4::new_one();
/// vec4::transform_mat2_mut(&mut v, &[1_f32, 0_f32, 0_f32, 1_f32]);
/// assert_eq!(v, vec4::new_one());
/// ```
#[inline]
pub fn transform_mat2_mut<'out, T>(out: &'out mut [T; 4], m: &[T; 4]) -> &'out mut [T; 4]
where
    T: Clone + Add<T, Output = T>,
    for<'a, 'b> &'a T: Mul<&'b T, Output = T>,
{
    let tmp = out.clone();
    transform_mat2(out, &tmp, m)
}
/// # Example
/// ```
/// let mut v = vec4::new_zero();
/// vec4::transform_mat3(&mut v, &vec4::new_one(),
///     &[1_f32, 0_f32, 0_f32, 0_f32, 1_f32, 0_f32, 0_f32, 0_f32, 1_f32],
/// );
/// assert_eq!(v, vec4::new_one());
/// ```
#[inline]
pub fn transform_mat3<'out, T>(out: &'out mut [T; 4], v: &[T; 4], m: &[T; 9]) -> &'out mut [T; 4]
where
    T: Clone + Add<T, Output = T>,
    for<'a, 'b> &'a T: Mul<&'b T, Output = T>,
{
    out[0] = &v[0] * &m[0] + &v[1] * &m[3] + &v[2] * &m[6];
    out[1] = &v[0] * &m[1] + &v[1] * &m[4] + &v[2] * &m[7];
    out[2] = &v[0] * &m[2] + &v[1] * &m[5] + &v[2] * &m[8];
    out[3] = v[3].clone();
    out
}
/// # Example
/// ```
/// let mut v = vec4::new_one();
/// vec4::transform_mat3_mut(&mut v,
///     &[1_f32, 0_f32, 0_f32, 0_f32, 1_f32, 0_f32, 0_f32, 0_f32, 1_f32],
/// );
/// assert_eq!(v, vec4::new_one());
/// ```
#[inline]
pub fn transform_mat3_mut<'out, T>(out: &'out mut [T; 4], m: &[T; 9]) -> &'out mut [T; 4]
where
    T: Clone + Add<T, Output = T>,
    for<'a, 'b> &'a T: Mul<&'b T, Output = T>,
{
    let tmp = out.clone();
    transform_mat3(out, &tmp, m)
}
/// # Example
/// ```
/// let mut v = vec4::new_zero();
/// vec4::transform_mat4(&mut v, &vec4::new_one(),
///     &[1_f32, 0_f32, 0_f32, 0_f32, 0_f32, 1_f32, 0_f32, 0_f32, 0_f32, 0_f32, 1_f32, 0_f32, 0_f32, 0_f32, 0_f32, 1_f32]
/// );
/// assert_eq!(v, vec4::new_one());
/// ```
#[inline]
pub fn transform_mat4<'out, T>(out: &'out mut [T; 4], v: &[T; 4], m: &[T; 16]) -> &'out mut [T; 4]
where
    T: Clone + Add<T, Output = T>,
    for<'a, 'b> &'a T: Mul<&'b T, Output = T>,
{
    out[0] = &v[0] * &m[0] + &v[1] * &m[4] + &v[2] * &m[8] + m[12].clone();
    out[1] = &v[0] * &m[1] + &v[1] * &m[5] + &v[2] * &m[9] + m[13].clone();
    out[2] = &v[0] * &m[2] + &v[1] * &m[6] + &v[2] * &m[10] + m[14].clone();
    out[3] = &v[0] * &m[3] + &v[1] * &m[7] + &v[2] * &m[11] + m[15].clone();
    out
}
/// # Example
/// ```
/// let mut v = vec4::new_one();
/// vec4::transform_mat4_mut(&mut v,
///     &[1_f32, 0_f32, 0_f32, 0_f32, 0_f32, 1_f32, 0_f32, 0_f32, 0_f32, 0_f32, 1_f32, 0_f32, 0_f32, 0_f32, 0_f32, 1_f32]
/// );
/// assert_eq!(v, vec4::new_one());
/// ```
#[inline]
pub fn transform_mat4_mut<'out, T>(out: &'out mut [T; 4], m: &[T; 16]) -> &'out mut [T; 4]
where
    T: Clone + Add<T, Output = T>,
    for<'a, 'b> &'a T: Mul<&'b T, Output = T>,
{
    let tmp = out.clone();
    transform_mat4(out, &tmp, m)
}
/// # Example
/// ```
/// let mut v = vec4::new_zero();
/// vec4::transform_mat4_rotation(&mut v, &vec4::new_one(),
///     &[1_f32, 0_f32, 0_f32, 0_f32, 0_f32, 1_f32, 0_f32, 0_f32, 0_f32, 0_f32, 1_f32, 0_f32, 0_f32, 0_f32, 0_f32, 1_f32]
/// );
/// assert_eq!(v, vec4::new_one());
/// ```
#[inline]
pub fn transform_mat4_rotation<'out, T>(
    out: &'out mut [T; 4],
    v: &[T; 4],
    m: &[T; 16],
) -> &'out mut [T; 4]
where
    T: Clone + Add<T, Output = T>,
    for<'a, 'b> &'a T: Mul<&'b T, Output = T>,
{
    out[0] = &v[0] * &m[0] + &v[1] * &m[4] + &v[2] * &m[8];
    out[1] = &v[0] * &m[1] + &v[1] * &m[5] + &v[2] * &m[9];
    out[2] = &v[0] * &m[2] + &v[1] * &m[6] + &v[2] * &m[10];
    out[3] = v[3].clone();
    out
}
/// # Example
/// ```
/// let mut v = vec4::new_one();
/// vec4::transform_mat4_rotation_mut(&mut v,
///     &[1_f32, 0_f32, 0_f32, 0_f32, 0_f32, 1_f32, 0_f32, 0_f32, 0_f32, 0_f32, 1_f32, 0_f32, 0_f32, 0_f32, 0_f32, 1_f32]
/// );
/// assert_eq!(v, vec4::new_one());
/// ```
#[inline]
pub fn transform_mat4_rotation_mut<'out, T>(out: &'out mut [T; 4], m: &[T; 16]) -> &'out mut [T; 4]
where
    T: Clone + Add<T, Output = T>,
    for<'a, 'b> &'a T: Mul<&'b T, Output = T>,
{
    let tmp = out.clone();
    transform_mat4(out, &tmp, m)
}
/// # Example
/// ```
/// let mut v = vec4::new_zero();
/// vec4::transform_mat4_projection(&mut v, &vec4::new_one(),
///     &[1_f32, 0_f32, 0_f32, 0_f32, 0_f32, 1_f32, 0_f32, 0_f32, 0_f32, 0_f32, 1_f32, 0_f32, 0_f32, 0_f32, 0_f32, 1_f32]
/// );
/// assert_eq!(v, vec4::new_one());
/// ```
#[inline]
pub fn transform_mat4_projection<'out, T>(
    out: &'out mut [T; 4],
    v: &[T; 4],
    m: &[T; 16],
) -> &'out mut [T; 4]
where
    T: Clone + One + Zero + Add<T, Output = T>,
    for<'a, 'b> &'a T: Div<&'b T, Output = T> + Mul<&'b T, Output = T>,
{
    let d = &v[0] * &m[3] + &v[1] * &m[7] + &v[2] * &m[11] + &v[3] * &m[15];
    let inv_d = if d.is_zero() { d } else { &T::one() / &d };

    out[0] = &(&v[0] * &m[0] + &v[1] * &m[4] + &v[2] * &m[8] + m[12].clone()) * &inv_d;
    out[1] = &(&v[0] * &m[1] + &v[1] * &m[5] + &v[2] * &m[9] + m[13].clone()) * &inv_d;
    out[2] = &(&v[0] * &m[2] + &v[1] * &m[6] + &v[2] * &m[10] + m[14].clone()) * &inv_d;
    out[3] = &(&v[0] * &m[3] + &v[1] * &m[7] + &v[2] * &m[11] + m[15].clone()) * &inv_d;
    out
}
/// # Example
/// ```
/// let mut v = vec4::new_one();
/// vec4::transform_mat4_projection_mut(&mut v,
///     &[1_f32, 0_f32, 0_f32, 0_f32, 0_f32, 1_f32, 0_f32, 0_f32, 0_f32, 0_f32, 1_f32, 0_f32, 0_f32, 0_f32, 0_f32, 1_f32]
/// );
/// assert_eq!(v, vec4::new_one());
/// ```
#[inline]
pub fn transform_mat4_projection_mut<'out, T>(
    out: &'out mut [T; 4],
    m: &[T; 16],
) -> &'out mut [T; 4]
where
    T: Clone + One + Zero + Add<T, Output = T>,
    for<'a, 'b> &'a T: Div<&'b T, Output = T> + Mul<&'b T, Output = T>,
{
    let tmp = out.clone();
    transform_mat4_projection(out, &tmp, m)
}
